<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Categories;
use App\Campaigns;

class Categories extends Model
{
    //
    protected $table = 'categories';

    public static function updateTotalCampaigns($cat_id)
    {
    	$count_campaigns = Campaigns::where('category_id', $cat_id)->count();
    	$categories = Categories::find($cat_id);
    	$categories->total_campaigns = $count_campaigns;
        $categories->save();
    }
}
