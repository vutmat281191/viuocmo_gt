<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Blogs;
use Illuminate\Http\Request;
use App\Http\Requests;

class ManageBlogsController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
		$blogs = Blogs::all();
		return view('admin.manageblogs.index', ['blogs' => $blogs]);
	}

	////////////////////////////-----manage blogs-----////////////////////////////
    public function createBlogs()
    {
        return view('admin.manageblogs.createBlogs');
    }

    public function submitCreateBlogs(Request $request)
    {
        $blogs = new Blogs();
        $blogs->title = $request->input('title');
        $blogs->content = $request->input('content');
        $blogs->save();
        return redirect('/admin/manageblogs');
    }

    public function editBlogs($id)
    {
        $blogs = Blogs::find($id);
        return view('admin.manageblogs.editBlogs', ['blogs' => $blogs]);
    }

    public function submitEditBlogs(Request $request)
    {
        $blogs = Blogs::find($request->input('id_blogs'));
        $blogs->title = $request->input('title');
        $blogs->content = $request->input('content');
        $blogs->save();
        return redirect('/admin/manageblogs');
    }

    public function deleteBlogs($id)
    {
        $blogs = Blogs::find($id);
        $blogs->delete();
        return redirect('/admin/manageblogs');
    }
}