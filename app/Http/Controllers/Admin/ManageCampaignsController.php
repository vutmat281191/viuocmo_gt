<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Campaigns;
use App\Categories;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;

class ManageCampaignsController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	$campaigns = Campaigns::all();
		return view('admin.managecampaigns.index', ['campaigns' => $campaigns]);
	}

	////////////////////////////-----manage campaign-----////////////////////////////
    public function createCampaign()
    {
        $categories = Categories::all();
        return view('admin.managecampaigns.createCampaign', ['categories' => $categories]);
    }

    public function submitCreateCampaign(Request $request)
    {
        // Handle upload file
        $newCampaign = new Campaigns();
        $newCampaign->type_upload = $request->input('type_upload');

        if ($request->input('type_upload') == 'photo' ) {
            if($request->hasFile('upload_photo'))
            {
                $photo = $request->file('upload_photo');
                $filename = time() . '.' . $photo->getClientOriginalExtension();
                Image::make($photo)->resize(600, 400)->save( public_path('/uploads/campaigns/' . $filename ) );

                $newCampaign->upload = $filename;
            }
        } else {
            $newCampaign->upload = $request->input('upload_url');
        }

        $newCampaign->user_id = Auth::id();
        $newCampaign->category_id = $request->input('category');
        $newCampaign->title = $request->input('title');
        $newCampaign->content = $request->input('content');
        $newCampaign->money_target = $request->input('money_target');
        $newCampaign->save();
        Categories::updateTotalCampaigns($request->input('category'));
        return redirect('/admin/managecampaigns');
    }

    public function editCampaign($id)
    {
        $categories = Categories::all();
        $campaign = Campaigns::find($id);
        return view('admin.managecampaigns.editCampaign', ['campaign' => $campaign, 'categories' => $categories]);
    }

    public function submitEditCampaign(Request $request)
    {
        $campaign = Campaigns::find($request->input('id_campaign'));
        $campaign->type_upload = $request->input('type_upload');

        if ($request->input('type_upload') == 'photo' ) {
            if($request->hasFile('upload_photo'))
            {
                $photo = $request->file('upload_photo');
                $filename = time() . '.' . $photo->getClientOriginalExtension();
                Image::make($photo)->resize(600, 400)->save( public_path('/uploads/campaigns/' . $filename ) );

                $campaign->upload = $filename;
            }
        } else {
            $campaign->upload = $request->input('upload_url');
        }

        // $campaign->user_id = Auth::id();
        $campaign->category_id = $request->input('category');
        $campaign->title = $request->input('title');
        $campaign->content = $request->input('content');
        $campaign->money_target = $request->input('money_target');
        $campaign->save();
        return redirect('/admin/managecampaigns');
    }

    public function deleteCampaign($id)
    {
        $campaign = Campaigns::find($id);
        Categories::updateTotalCampaigns($campaign->category_id);
        $campaign->delete();
        return redirect('/admin/managecampaigns');
    }
}