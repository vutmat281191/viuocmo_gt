<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Categories;
use App\Http\Requests\ValidateCategories;
use Illuminate\Http\Request;
use App\Http\Requests;

class ManageCategoriesController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	$categories = Categories::all();
    	// dd($categories);exit();
		return view('admin.managecategories.index', ['categories' => $categories]);
	}

	////////////////////////////-----manage category-----////////////////////////////
    public function createCategory()
    {
        return view('admin.managecategories.createCategory');
    }

    public function submitCreateCategory(Request $request)
    {
        $category = new Categories();
        $category->title = $request->input('title');
        $category->content = $request->input('content');
        $category->save();
        return redirect('/admin/managecategories');
    }

    public function editCategory($id)
    {
        $category = Categories::find($id);
        return view('admin.managecategories.editCategory', ['category' => $category]);
    }

    public function submitEditCategory(Request $request)
    {
        $category = Categories::find($request->input('id_category'));
        $category->title = $request->input('title');
        $category->sub_content = $request->input('sub_content');
        $category->content = $request->input('content');
        $category->save();
        return redirect('/admin/managecategories');
    }

    public function deleteCategory($id)
    {
        // dd('dfdfdf');exit();
        // $category = Categories::find($id);
        // $category->delete();
        return redirect('/admin/managecategories');
    }
}