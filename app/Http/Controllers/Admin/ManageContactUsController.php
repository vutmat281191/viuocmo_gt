<?php
namespace App\Http\Controllers\Admin;

use App\ContactUs;
use App\Http\Controllers\Controller;

class ManageContactUsController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	$messages = ContactUs::all();
		return view('admin.managecontactus.index', ['messages' => $messages]);
	}

	public function deleteMessage($id)
	{
		$messages = ContactUs::find($id);
        $messages->delete();
        return redirect('/admin/managecontactus');
	}

}