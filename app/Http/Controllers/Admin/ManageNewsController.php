<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\News;
use Illuminate\Http\Request;
use App\Http\Requests;

class ManageNewsController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	$news = News::all();
		return view('admin.managenews.index', ['news' => $news]);
	}

	////////////////////////////-----manage News-----////////////////////////////
    public function createNews()
    {
        return view('admin.managenews.createNews');
    }

    public function submitCreateNews(Request $request)
    {
        $news = new News();
        $news->title = $request->input('title');
        $news->content = $request->input('content');
        $news->save();
        return redirect('/admin/managenews');
    }

    public function editNews($id)
    {
        $news = News::find($id);
        return view('admin.managenews.editNews', ['news' => $news]);
    }

    public function submitEditNews(Request $request)
    {
        $news = News::find($request->input('id_news'));
        $news->title = $request->input('title');
        $news->content = $request->input('content');
        $news->save();
        return redirect('/admin/managenews');
    }

    public function deleteNews($id)
    {
        $news = News::find($id);
        $news->delete();
        return redirect('/admin/managenews');
    }
}