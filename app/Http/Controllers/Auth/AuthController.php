<?php
namespace App\Http\Controllers\Auth;

use App\User;
use App\Categories;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
// use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RegistersUsers;
// use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Response;
use Auth;
use Image;
use Exception;
use Socialite;
use Validator;

class AuthController extends Controller
{

	use  ThrottlesLogins;

	protected $redirectTo = '/';

	public function __construct()
	{
		$categories = Categories::all();
		view()->share('categories', $categories);
		$this->middleware('guest', ['except' => 'logout']);
	}

	protected function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
		]);
	}

	protected function downloadfile($url, $filename)
	{
		$tempImage = tempnam(public_path('/uploads/avatars/'), $filename);
		copy($url, $tempImage);

		return response()->download($tempImage, $filename);
	}

	public function redirectToGoogle()
	{
		return Socialite::driver('google')->redirect();
	}

	public function handleGoogleCallback()
 {
  $user = Socialite::driver('google')->user();
  $res = User::where('email', $user->email)->first();
  // dd($res); exit();
  if (!$res) {
   try {
    $name = $user->name;
    $email = $user->email;
    $time = time() . '.jpg';

    //down image
    Image::make($user->avatar)->resize(300, 300)->save(public_path('/uploads/avatars/' . $time));
    
    $userModel = new User;
    $userModel->name = $name;
    $userModel->email = $email;
    $userModel->google_id = $user->id;
    $userModel->fullname = $user->name;
    $userModel->password = bcrypt(123456);
    $userModel->avatar = $time;
    $userModel->role_id = 2;
    $userModel->save();

    //send mail
    $mail = \Mail::send(
     'users.mails.welcomeByGoogle',
     [ 'firstname'=> $name ],
     function($message) use ($email){
      $message->to($email)->subject('Welcome to the viuocmo.com!');
     }
    );


    //login
    Auth::loginUsingId($userModel->id);
    
    //redirect
    return redirect('/');
   } catch (Exception $e) {
    dd($e);
    exit();
    return redirect('/');
   }
  } else {
   Auth::loginUsingId($res->id);
   return redirect('/');
  }
 }
}