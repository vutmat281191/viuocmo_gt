<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\CreateCampaign;
use Auth;
use Image;
use App\Categories;
use App\Campaigns;
use Validator;

class CampaignController extends Controller
{
	public function __construct()
	{
		$categories = Categories::all();
		view()->share('categories', $categories);
	}

	public function submitCreateCampaign(CreateCampaign $request)
	{
		//validation
		// $v = Validator::make($request->all(),
		// 	[
		// 		'type_upload' => 'required',
		// 		'category' => 'required',
		// 		'title' => 'required',
		// 		'content' => 'required',
		// 		'money_target' => 'required',
		// 	],
		// 	[
		// 		'type_upload.required' => 'Vui Lòng chọn type upload',
		// 		'category.required' => 'Vui Lòng chọn category',
		// 		'title.required' => 'Vui Lòng chọn title',
		// 		'content.required' => 'Vui Lòng chọn content',
		// 		'money_target.required' => 'Vui Lòng chọn target',
		// 	]
		// );
		// if ($v->fails()) {
		// 	return redirect()->back()->withErrors($v->Errors());
		// }
		// Handle upload file
		$newCampaign = new Campaigns();
		$newCampaign->type_upload = $request->input('type_upload');

		if ($request->input('type_upload') == 'photo' ) {
			if($request->hasFile('upload_photo'))
			{
				$photo = $request->file('upload_photo');
				$filename = time() . '.' . $photo->getClientOriginalExtension();
				Image::make($photo)->resize(600, 400)->save( public_path('/uploads/campaigns/' . $filename ) );

				$newCampaign->upload = $filename;
			}
		} else {
			$newCampaign->upload = $request->input('upload_url');
		}

		$newCampaign->user_id = Auth::id();
		$newCampaign->category_id = $request->input('category');
		$newCampaign->title = $request->input('title');
		$newCampaign->content = $request->input('content');
		$newCampaign->money_target = $request->input('money_target');
		$newCampaign->link = md5(time().Auth::id());
		$newCampaign->save();
		Categories::updateTotalCampaigns($request->input('category'));
		return redirect('/campaign/' . $newCampaign->link);
	}

	public function changeStatusCampaign (Request $request)
	{
		$campaign = Campaigns::find($request->campaign_id);
		if ($campaign->status == 'active') {
			$campaign->status = 'deactive';
		} else {
			$campaign->status = 'active';
		}
		if ($campaign->save()) {
			return response()->json(array('msg'=> 'update success'), 200);
		} else {
			return response()->json(array('msg'=> 'update fail'), 404);
		}
	}

	public function searchCampaign (Request $request)
	{
		if (!empty($request->input('search'))) {
			$queryString = $request->input('search');
			$builder = Campaigns::query();
			$builder->where('title', 'LIKE', "%$queryString%");
			$res = $builder->orderBy('title')->paginate(5);
			// dd($res->count());exit();

			return view('pages.searchCampaign', ['count' => $res->count(), 'list' => $res, 'text_search' => $queryString]);
		}
	}
}
