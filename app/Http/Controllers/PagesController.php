<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Campaigns;
use App\News;
use App\Blogs;
use Auth;

class PagesController extends Controller
{
	public function __construct()
	{
		$categories = Categories::all();
		view()->share('categories', $categories);
	}

	public function home()
	{
		return view('pages.home');
	}

	public function categories($id)
	{
		$campaigns = Campaigns::where('category_id', $id)->get();
		return view('pages.categories', ['id' => $id, 'campaigns' => $campaigns]);
	}

	public function campaign($id)
	{
		$campaign = Campaigns::where('link', $id)->first();
		$url_media = '';
		if ($campaign->type_upload == 'photo') {
			$url_media = '<img class="img_campaign" src="' . url("public/uploads/campaigns/" . $campaign->upload) . '">';
		} else {
			$url_media = '<div class="embed-responsive embed-responsive-16by9 col-xs-12 text-center">' . $campaign->upload . '</div>';
		}
		return view('pages.campaign', ['campaign' => $campaign, 'url_media' => $url_media]);
	}

	public function tour()
	{
		return view('pages.tour');
	}

	public function fee()
	{
		return view('pages.fee');
	}

	public function aboutUs()
	{
		return view('pages.aboutUs');
	}

	public function hiring()
	{
		return view('pages.hiring');
	}

	public function contactUs()
	{
		return view('pages.contactUs');
	}

	public function terms()
	{
		return view('pages.terms');
	}

	public function privacy()
	{
		return view('pages.privacy');
	}

	public function legal()
	{
		return view('pages.legal');
	}

	public function listNews()
	{
		$news = News::all();
		return view('pages.listNews', ['news' => $news]);
	}

	public function news($id)
	{
		$new = News::find($id);
		return view('pages.news', ['new' => $new]);
	}

	public function listBlogs()
	{
		$blogs = Blogs::all();
		return view('pages.listBlogs', ['blogs' => $blogs]);
	}

	public function blogs($id)
	{
		$blog = Blogs::find($id);
		return view('pages.blogs', ['blog' => $blog]);
	}

	public function myAccount()
	{
		if(Auth()->check()){
			return view('pages.myAccount', ['user' => Auth::user()] );
		}
		return redirect('/login');
	}

	public function myCampaigns()
	{
		if(Auth()->check()){
			$campaigns = Campaigns::where('user_id', Auth::id())->get();
			return view('pages.myCampaigns', ['campaigns' => $campaigns] );
		}
		return redirect('/login');
	}

	public function pageCreateCampaign()
	{
		if(Auth()->check()){
			return view('pages.createCampaign');
		}
		return redirect('/login');
	}
}
