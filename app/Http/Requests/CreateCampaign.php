<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCampaign extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			//
			'type_upload' => 'required',
			'money_target' => 'required|numeric|min:500000',
			'title' => 'required',
			'category' => 'required',
			'content' => 'required',
		];
	}

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'type_upload.required' => 'type upload is required',
			'money_target.required'  => 'target is required',
			'money_target.numeric'  => 'target is numberic',
			'money_target.min'  => 'minimum is 500.000vnd',
			'title.required'  => 'title is required',
			'category.required'  => 'category is required',
			'content.required'  => 'content is required',
		];
	}
}
