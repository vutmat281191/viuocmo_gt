-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Apr 13, 2018 at 07:27 AM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gt_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default-blog.jpg',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `title`, `content`, `image`, `created_at`, `updated_at`) VALUES
(1, 'How You Can Join Greta Van Susteren in Saving Lives in Liberia', '<p>It would have been easy to stop there. But now Greta has a more ambitious goal: to provide Sampson’s home country of Liberia with a CT scanner to save even more lives.</p>\r\n\r\n<p>Surprising as it may be, the entire country of Liberia is without this crucial piece of medical equipment. Because of limitations like this, cases such as Sampson’s progress far beyond what they should because proper medical intervention can’t be made.</p>\r\n\r\n<p><strong>Greta wants to change this</strong>. <a href="https://www.gofundme.com/CT-Liberia" data-href="https://www.gofundme.com/CT-Liberia" class="markup--anchor markup--p-anchor" rel="noopener" target="_blank">And with her new GoFundMe</a>, already $88,000 has been raised out of an formidable goal of $275,000. But there’s still a way to go to ensure the success of this critical project.</p>\r\n\r\n<p>A donation to this campaign isn’t just a generous gift&#8202;—&#8202;it’s also a vital investment in the health and livelihood of thousands of people who, through the random circumstances of their birth, don’t have access to a technology many of us take for granted.</p>\r\n\r\n<p>The campaign to save Sampson’s life showed the power of a group of people to make a real difference. It’s time to do it again&#8202;—&#8202;and have a lasting impact on an entire country.</p>', '1.jpg', '2018-01-02 17:00:00', '2018-01-16 17:00:00'),
(2, 'This is title', '<p>It would have been easy to stop there. But now Greta has a more ambitious goal: to provide Sampson’s home country of Liberia with a CT scanner to save even more lives.</p>\r\n\r\n<p>Surprising as it may be, the entire country of Liberia is without this crucial piece of medical equipment. Because of limitations like this, cases such as Sampson’s progress far beyond what they should because proper medical intervention can’t be made.</p>\r\n\r\n<p><strong>Greta wants to change this</strong>. <a href="https://www.gofundme.com/CT-Liberia" data-href="https://www.gofundme.com/CT-Liberia" class="markup--anchor markup--p-anchor" rel="noopener" target="_blank">And with her new GoFundMe</a>, already $88,000 has been raised out of an formidable goal of $275,000. But there’s still a way to go to ensure the success of this critical project.</p>\r\n\r\n<p>A donation to this campaign isn’t just a generous gift&#8202;—&#8202;it’s also a vital investment in the health and livelihood of thousands of people who, through the random circumstances of their birth, don’t have access to a technology many of us take for granted.</p>\r\n\r\n<p>The campaign to save Sampson’s life showed the power of a group of people to make a real difference. It’s time to do it again&#8202;—&#8202;and have a lasting impact on an entire country.</p>', '2.jpg', '2018-01-02 17:00:00', '2018-01-22 17:00:00'),
(3, 'this is another title', '<p>It would have been easy to stop there. But now Greta has a more ambitious goal: to provide Sampson’s home country of Liberia with a CT scanner to save even more lives.</p>\r\n\r\n<p>Surprising as it may be, the entire country of Liberia is without this crucial piece of medical equipment. Because of limitations like this, cases such as Sampson’s progress far beyond what they should because proper medical intervention can’t be made.</p>\r\n\r\n<p><strong>Greta wants to change this</strong>. <a href="https://www.gofundme.com/CT-Liberia" data-href="https://www.gofundme.com/CT-Liberia" class="markup--anchor markup--p-anchor" rel="noopener" target="_blank">And with her new GoFundMe</a>, already $88,000 has been raised out of an formidable goal of $275,000. But there’s still a way to go to ensure the success of this critical project.</p>\r\n\r\n<p>A donation to this campaign isn’t just a generous gift&#8202;—&#8202;it’s also a vital investment in the health and livelihood of thousands of people who, through the random circumstances of their birth, don’t have access to a technology many of us take for granted.</p>\r\n\r\n<p>The campaign to save Sampson’s life showed the power of a group of people to make a real difference. It’s time to do it again&#8202;—&#8202;and have a lasting impact on an entire country.</p>', '3.jpg', NULL, NULL),
(4, 'this is another title again', '<p>It would have been easy to stop there. But now Greta has a more ambitious goal: to provide Sampson’s home country of Liberia with a CT scanner to save even more lives.</p>\r\n\r\n<p>Surprising as it may be, the entire country of Liberia is without this crucial piece of medical equipment. Because of limitations like this, cases such as Sampson’s progress far beyond what they should because proper medical intervention can’t be made.</p>\r\n\r\n<p><strong>Greta wants to change this</strong>. <a href="https://www.gofundme.com/CT-Liberia" data-href="https://www.gofundme.com/CT-Liberia" class="markup--anchor markup--p-anchor" rel="noopener" target="_blank">And with her new GoFundMe</a>, already $88,000 has been raised out of an formidable goal of $275,000. But there’s still a way to go to ensure the success of this critical project.</p>\r\n\r\n<p>A donation to this campaign isn’t just a generous gift&#8202;—&#8202;it’s also a vital investment in the health and livelihood of thousands of people who, through the random circumstances of their birth, don’t have access to a technology many of us take for granted.</p>\r\n\r\n<p>The campaign to save Sampson’s life showed the power of a group of people to make a real difference. It’s time to do it again&#8202;—&#8202;and have a lasting impact on an entire country.</p>', '4.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `campaigns`
--

CREATE TABLE IF NOT EXISTS `campaigns` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `upload` varchar(5000) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default_campaign.jpg',
  `type_upload` enum('url_youtube','photo') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'photo',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `money_target` bigint(20) NOT NULL,
  `money_current` bigint(20) DEFAULT NULL,
  `status` enum('active','deactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `link` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=61 ;

--
-- Dumping data for table `campaigns`
--

INSERT INTO `campaigns` (`id`, `user_id`, `category_id`, `upload`, `type_upload`, `title`, `content`, `money_target`, `money_current`, `status`, `link`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'default_campaign.jpg', 'photo', 'sdfdf', '<p>sdgdfg</p>', 2345345345, NULL, 'active', '49810de46a6bb771565d43a751773e58', '2017-11-06 07:02:02', '2017-11-11 19:28:59'),
(2, 1, 5, 'default_campaign.jpg', 'photo', 'rtertg45g', '<p>3tergrtg</p>\r\n<p>hj,jkl;</p>\r\n<p>dfgh</p>', 345345, NULL, 'deactive', 'df43f69cca2f4591c640af9788d3c62e', '2017-11-06 07:12:03', '2017-11-11 19:28:32'),
(3, 1, 2, 'default_campaign.jpg', 'photo', 'Stefan Karl''s Year of Healing', '<p>As Robbie Rotten on LazyTown,  has given laughter to children around the world. He created a foundation to help stop bullying.  He is a tireless advocate for special needs children. \r\n\r\nAnd now Stefan Karl needs our help. \r\n\r\nStefan has been diagnosed with pancreatic cancer. The operation he has had will prevent him from working for up to a year.\r\n\r\nWe want him to use that year to rest and recuperate so that he can once again return to what he does best - making kids smile all over planet Earth. \r\n\r\nWe are raising funds to help Stefan and his family survive the coming year.  Not only will they need assistance with medical bills, but it''s important that Stefan be allowed to heal his body and spirit without the additional burden of financial need. \r\n\r\nWe appreciate any amount of gift you are able to extend to Stefan and his family. As he moves from this difficult time toward a full recovery, we ask for your generosity, your prayers and good wishes. \r\n\r\nAs Robbie Rotten would ask, "Why are those people making all that noise!?" \r\n\r\nLet''s answer Robbie with a thunderous noise of gratitude and joy for everything he''s given to the world of children''s entertainment!\r\n\r\nHelp spread the word!</p>', 435345345, NULL, 'active', 'cc20c9c3d62aca7789c3f81aaea749f3', '2017-11-10 15:46:36', '2017-11-10 15:46:36'),
(4, 1, 2, '1510355164.jpg', 'photo', 'Toddy''s Surgery Fund', '<p>Tod is a 7 month old chow chow born with a birth defect called "sever hip dysplacia".  Long story short, by the time he turns 1, he wont be able to walk, run, let alone play.  \r\nThis condition presents us with two options:\r\n1: is to put him down ( which I refuse to do)\r\n2: let him go through a very specialized surgery.\r\n\r\nThe surgery however, will cost around $8000. So I am humbley asking you to donate anything.  Anything would be greatly appreciated.</p>', 343463563, NULL, 'active', '09d1c337b56d9a997d8fe55414c2ad13', '2017-11-10 16:06:05', '2017-11-10 16:06:05'),
(5, 1, 2, '<iframe width="854" height="480" src="https://www.youtube.com/embed/ILP9oW9OyxM" frameborder="0" gesture="media" allowfullscreen></iframe>', 'url_youtube', 'weweff', '<p>dsfsfsdfs</p>\r\n<p>hd</p>\r\n<p>fhfghfhfghfgh</p>', 44444, NULL, 'active', 'ca6b83167672792f9685dadde44a0fed', '2017-11-11 03:09:26', '2017-11-11 03:09:26'),
(6, 7, 2, '<iframe src="https://www.youtube.com/embed/ILP9oW9OyxM?ecver=2" style="position:absolute;width:100%;height:100%;left:0" width="641" height="360" frameborder="0" gesture="media" allowfullscreen></iframe>', 'url_youtube', 'feevdfvfvfv', '<p>fdfbdbdfbdfbdfbd</p>\r\n<p>fbdfbdfbdfb</p>\r\n<p>bdfbdfbdbf</p>', 324234234, NULL, 'active', '35246bc3d424c74bc9c8ab39517905c7', '2017-11-13 16:11:06', '2017-11-13 16:11:06'),
(7, 7, 2, '<iframe src="https://www.youtube.com/embed/avAnBSGUdC4?ecver=2" style="position:absolute;width:100%;height:100%;left:0" width="641" height="360" frameborder="0" gesture="media" allowfullscreen></iframe>', 'url_youtube', 'tytrhrthrth', '<p>rthrthrthr</p>\r\n<p>thrthrth</p>\r\n<p>rthrth</p>', 54345345, NULL, 'active', '0c2cc50f3ac139883a04676bcadda96c', '2017-11-13 16:13:35', '2017-11-13 16:13:35'),
(8, 7, 9, '1510615262.jpg', 'photo', 'MATW Africa Project with Ali Banat', '<p>MATW- Muslims Around The World was a special project developed in October 2015 to assist those less fortunate in the poverty stricken areas of Togo in Africa. “Muslims Around the World” was established by Ali Banat, a brother from within the Australian Muslim community, who recently found out he was diagnosed with cancer. This motivated Ali to travel to Togo, Africa and spend the rest of his time assisting the communities there.\r\n\r\n\r\nDuring his 2 week journey, Ali met local children and families and has seen their situation first hand. What started as a personal project has turned into a long term vision to bring joy to the smiles of children all over the world. Muslims Around the World aims to build a village which will be home to over 200 widows, a masjid for the local community and a school to house 600 orphans. A mini hospital/medical centre and businesses to support the local community are also going to be established. With Muslims Around the World, 100% of the donations will be delivered with 0% administrative fee as all fees are covered by sponsorship.\r\n\r\nAll funds collected are divided and spent amongst the 3 projects MATW has running at the moment MATW Village, Chive Project and Cemetry Project. We purchase all materials for all three sites, pay workers, provide food for those on sight and aid for those who in need also. In creating these 3 projects we have created on going sustainable support.</p>\r\n', 45345345, NULL, 'active', '9d376bc737a5fcd9f5a1a33b0857be97', '2017-11-13 16:21:02', '2017-11-13 16:21:02'),
(9, 7, 8, '1510615282.jpeg', 'photo', 'gghgfghfghfdsaffqwr', '<p>wegerhgerh</p>', 346456456, NULL, 'active', 'c24159e5582dfa713e0149b292bb1edd', '2017-11-13 16:21:22', '2017-11-13 16:21:22'),
(10, 7, 7, '1510615465.jpg', 'photo', 'ergergerh', '<p>herthrth</p>', 45354, NULL, 'active', 'a93cdee66d3ec10bbb80a064c259baee', '2017-11-13 16:24:25', '2017-11-13 16:24:25'),
(11, 7, 6, '1510615490.jpg', 'photo', 'erehreherh', '<p>rhrthrthrthrthrth</p>', 45353, NULL, 'active', 'b527c6c40809875bffdf00c9167698a7', '2017-11-13 16:24:50', '2017-11-13 16:24:50'),
(12, 7, 5, '1510615515.jpg', 'photo', 'rtertert', '<p>hjlhghmfghdf</p>', 36456456, NULL, 'active', '22c205251d09c8ade3e360832eba3a1d', '2017-11-13 16:25:15', '2017-11-13 16:25:15'),
(13, 7, 4, '1510615547.jpg', 'photo', 'trhrthrt', '<p>nfgbfbfb</p>', 56456456, NULL, 'active', '6800fbe5fcc1855e3c331f72e339e222', '2017-11-13 16:25:49', '2017-11-13 16:25:49'),
(14, 7, 3, 'default_campaign.jpg', 'photo', 'rtuuewetsgdbvg', '<p>hdfhfghfghy5hrth</p>', 56456456, NULL, 'active', 'aa23a7c68a20ca5c2388ebd0e43764c3', '2017-11-13 16:26:10', '2017-11-13 16:26:10'),
(15, 7, 2, '1510615590.jpg', 'photo', 'htrghrhfg', '<p>fghfgh</p>', 5345345, NULL, 'active', 'ebe7be182c1bdc3e61e74f2eb7018ab1', '2017-11-13 16:26:31', '2017-11-13 16:26:31'),
(16, 7, 1, '1510615612.jpg', 'photo', 'rthrthrth', '<p>rthrtrthdsdgzvxc</p>', 54545, NULL, 'active', '81e27d0151767bfd2ee504e199aae7a8', '2017-11-13 16:26:52', '2017-11-13 16:26:52'),
(17, 1, 2, '1510668415.jpg', 'photo', 'ett34t34t', '<p>t34t34t</p>', 34234, NULL, 'active', '5ab3f742411e6f292eee70f1f0c75eb4', '2017-11-14 07:06:57', '2017-11-14 07:06:57'),
(18, 1, 9, '1510668450.jpg', 'photo', 't4t43t34t', '<p>24t34t</p>', 33443, NULL, 'active', '08bca1dad86db38a9c49326c835c0a97', '2017-11-14 07:07:31', '2017-11-14 07:07:31'),
(19, 1, 8, '1510668467.jpg', 'photo', '34t34t34t', '<p>34t34t34t34t34t</p>', 243434, NULL, 'active', 'ff413bb11bffcaf68912345b02c810be', '2017-11-14 07:07:48', '2017-11-14 07:07:48'),
(20, 1, 3, '1510668485.jpg', 'photo', '34t34t34t', '<p>76767j67j67uu67u67u</p>', 343434, NULL, 'active', 'b349ef8afee409877c6e3c6bad397389', '2017-11-14 07:08:05', '2017-11-14 07:08:05'),
(21, 1, 7, '1510668504.jpeg', 'photo', '67k67k67k', '<p>67k67k67k67k</p>', 67676767, NULL, 'active', '96575a3ce1c629c3e53ab85c8618137b', '2017-11-14 07:08:25', '2017-11-14 07:08:25'),
(22, 1, 1, '1510668523.jpg', 'photo', '8989l89l89l89l', '<p>89l89l89l89l</p>', 898989, NULL, 'active', '60c6ee4f862c4bba6bb36297d05983be', '2017-11-14 07:08:44', '2017-11-14 07:08:44'),
(23, 1, 5, '1510668538.jpg', 'photo', 'l89l89l89l', '<p>98l89l89l89l</p>', 988989, NULL, 'active', 'a694415ce1ac21a115644d49113e16f4', '2017-11-14 07:08:58', '2017-11-14 07:08:58'),
(24, 1, 4, '1510668555.jpg', 'photo', '56h56h45h', '<p>45h56h45h</p>', 8989896767, NULL, 'active', '8691b1d9dfbf9fa55e0ebe2a56fcedc5', '2017-11-14 07:09:15', '2017-11-14 07:09:15'),
(25, 1, 5, '1510668570.jpg', 'photo', 'j56j4w5h44h', '<p>6uj3564j56j45j6</p>', 565656, NULL, 'active', '68e1bed9e1dd1290ef76b4741b2976c8', '2017-11-14 07:09:31', '2017-11-14 07:09:31'),
(26, 1, 6, '1510668610.jpg', 'photo', 'gẻgerg22', '<p>qqwdweffgsdvs</p>', 33433443, NULL, 'active', 'c4313ef2fd226e803b273b63de583e89', '2017-11-14 07:10:11', '2017-11-14 07:10:11'),
(27, 1, 6, '1511000268.jpg', 'photo', '34 34 34 34 34', '<p>1413r2313</p>\r\n<p>132523523</p>', 111111, NULL, 'active', 'a7a9cc2b54234a81480ea150d7e9562c', '2017-11-18 03:17:49', '2017-11-18 03:17:49'),
(28, 1, 9, '1511000298.jpg', 'photo', '34235235', '<p>gsdfhkahf</p>\r\n<p>dfjjkhsehnf</p>', 45345, NULL, 'active', '3edf3c99623cd0da17c4eca2e74fe40a', '2017-11-18 03:18:19', '2017-11-18 03:18:19'),
(29, 1, 4, '1511000345.jpg', 'photo', '3R12', '<p>Fegarg</p>\r\n<p>ửahaeh</p>', 223, NULL, 'active', 'e1a002451c59331d6e425b8827a9ee7d', '2017-11-18 03:19:05', '2017-11-18 03:19:05'),
(30, 1, 7, '<iframe width="640" height="360" src="https://www.youtube.com/embed/yPQDl9xqKfM" frameborder="0" gesture="media" allowfullscreen></iframe>', 'url_youtube', '3t34657', '<p>fsdfsdf</p>\r\n<p>sdfsdfsdf</p>', 4244, NULL, 'active', 'a0d8f5b0d5db761005aeaa57ede9cf4e', '2017-11-18 05:03:17', '2017-11-18 05:03:17'),
(31, 1, 7, '1511735516.jpg', 'photo', '345345', '<p>45345</p>', 53453, NULL, 'active', '241b06de183ca5b2d14ea46eec89b0f5', '2017-11-26 15:31:56', '2017-11-26 15:31:56'),
(32, 1, 5, '1511735548.jpg', 'photo', '345345', '<p>35345</p>', 345345, NULL, 'active', 'c671259cbc1ec5c684441cff160d9dd1', '2017-11-26 15:32:28', '2017-11-26 15:32:28'),
(33, 18, 4, '1512655799.jpg', 'photo', 'ểtrtertert', '<p>ểtrtertert</p>\r\n<p>ểtrtert</p>', 45345300000, NULL, 'active', 'c671259cbc1ec5c684441cff160d9dd6', '2017-12-07 07:10:00', '2017-12-07 07:10:00'),
(34, 18, 6, '1513096777.jpg', 'photo', '888888888888888', '<p>65756756756756757</p>', 888000000, NULL, 'active', 'c671259cbc1ec5c684441cff160d9dd9', '2017-12-12 09:39:37', '2017-12-12 09:39:37'),
(35, 18, 2, '1513096860.jpg', 'photo', '858568586568', '<p>568568568</p>', 888000000, NULL, 'active', 'c671259cbc1ec5c684441cff160d9dd0', '2017-12-12 09:41:00', '2017-12-12 09:41:00'),
(36, 18, 3, '1513096970.jpg', 'photo', '77878787878787', '<p>8678678678676</p>', 77777700000, NULL, 'active', 'c671259cbc1ec5c684441cff160d9d89', '2017-12-12 09:42:51', '2017-12-12 09:42:51'),
(37, 18, 6, '1513097039.jpg', 'photo', '47u667u67u67u67', '<p>56u55i6565656i5i6</p>', 7777000000, NULL, 'active', 'c671259cbc1ec5c684441cff160d9d56', '2017-12-12 09:43:59', '2017-12-12 09:43:59'),
(38, 18, 2, '1513097101.jpg', 'photo', '3463463', '<p>346346</p>', 434634600000, NULL, 'active', 'c671259cbc1ec5c684441cff160d9666', '2017-12-12 09:45:01', '2017-12-12 09:45:01'),
(39, 18, 3, '1513097170.jpg', 'photo', 'rgherhe', '<p>sdgsdg</p>', 45345349000000, NULL, 'active', 'fa8c297770ffb3db7a87956f34165ba6', '2017-12-12 09:46:10', '2017-12-12 09:46:10'),
(40, 18, 3, '1513097197.jpg', 'photo', 'rgherhe', '<p>sdgsdg</p>', 45345349000000, NULL, 'active', 'fa8c297770ffb3db7a87956f34165ba8', '2017-12-12 09:46:37', '2017-12-12 09:46:37'),
(41, 18, 3, '1513097231.jpg', 'photo', 'rgherhe', '<p>sdgsdg</p>', 45345349000000, NULL, 'active', 'fa8c297770ffb3db7a87956f34165ba9', '2017-12-12 09:47:11', '2017-12-12 09:47:11'),
(42, 18, 3, '1513097246.jpg', 'photo', 'rgherhe', '<p>sdgsdg</p>', 45345349000000, NULL, 'active', 'fa8c297770ffb3db7a87956f34165b12', '2017-12-12 09:47:27', '2017-12-12 09:47:27'),
(43, 18, 3, '1513097257.jpg', 'photo', 'rgherhe', '<p>sdgsdg</p>', 45345349000000, NULL, 'active', 'fa8c297770ffb3db7a87956f34165b34', '2017-12-12 09:47:37', '2017-12-12 09:47:37'),
(44, 18, 1, '1513097295.jpg', 'photo', '007806807680', '<p>6076067</p>', 6660000000, NULL, 'active', 'fa8c297770ffb3db7a87956f34165b56', '2017-12-12 09:48:15', '2017-12-12 09:48:15'),
(45, 18, 2, '1513097444.jpg', 'photo', '6u5u56565', '<p>56i56i</p>', 6600000000, NULL, 'active', 'fa8c297770ffb3db7a87956f34165b89', '2017-12-12 09:50:44', '2017-12-12 09:50:44'),
(46, 18, 4, '1513097463.jpg', 'photo', '45u546u556u', '<p>u56u56u</p>', 6556567000000, NULL, 'active', 'fa8c297770ffb3db7a87956f34165ba9', '2017-12-12 09:51:03', '2017-12-12 09:51:03'),
(47, 18, 5, '1513097489.jpg', 'photo', '675u6j56j56j', '<p>65k67kt7k</p>', 34456000000, NULL, 'active', '527db214d00e366102421d79a16d3b23', '2017-12-12 09:51:30', '2017-12-12 09:51:30'),
(48, 18, 6, '1513097507.jpg', 'photo', '56iui556i', '<p>45u45u</p>', 656700000, NULL, 'active', '527db214d00e366102421d79a16d3b13', '2017-12-12 09:51:47', '2017-12-12 09:51:47'),
(49, 18, 7, '1513097523.jpg', 'photo', '5i56i56i56i', '<p>56i5i65i6</p>', 45457457000000, NULL, 'active', '527db214d00e366102421d79a16d3b67', '2017-12-12 09:52:04', '2017-12-12 09:52:04'),
(50, 18, 7, '1513097540.jpg', 'photo', '6i56i5i656', '<p>56i56i5i656</p>', 657567000000, NULL, 'active', '527db214d00e366102421d79a16d3b78', '2017-12-12 09:52:20', '2017-12-12 09:52:20'),
(51, 18, 9, '1513097558.jpg', 'photo', '77567567utyututyu', '<p>656568</p>', 657567000000, NULL, 'active', '527db214d00e366102421d79a16d3b66', '2017-12-12 09:52:38', '2017-12-12 09:52:38'),
(52, 18, 8, '1513097594.jpg', 'photo', 'fef43t34g34', '<p>ưegeg</p>', 4534545600000000, NULL, 'active', '527db214d00e366102421d79a16d3b99', '2017-12-12 09:53:14', '2017-12-12 09:53:14'),
(53, 18, 1, '1513432536.jpg', 'photo', 'title', '<p>k&eacute;mdfsdf</p>\r\n<p>sdfsdsdg</p>\r\n<p>fđf</p>', 600000, NULL, 'active', '527db214d00e366102421d79a16d3b00', '2017-12-16 06:55:36', '2017-12-16 06:55:36'),
(54, 18, 7, '1514036407.jpg', 'photo', 'rtertert', '<p>rertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke g&nbsp; egk egk wkeg kw egk wke g rertrtert erg erg erg ekrgk g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke g sfdjhjkwhefhweetwekntt4trertrtert erg erg erg ekrg k g ejkbr ke bkj erkgwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke g ehrjrsjer 5jrtjt&nbsp;rertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egkwkeg kw egk wke g rt rtjrtu r6rertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke g 6 u6 u65 u56rertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke g&nbsp; u56 u56 u56urertrtert erg erg erg ekrg k g ejkbrkebkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke g56 u56 urertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke g56 u56 u65rertrtert ergerg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke g u56 u56u</p>\r\n<p>rertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke&nbsp;rertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg ewrgk wkejg kw egk wkeg kw egk wke grertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke grertrtert erg ergerg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke grertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgkwkejgkw egk wkeg kw egk wke grertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke grertrtert erg ergergekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke grertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejgkw egk wkeg kw egk wke grertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke g&nbsp;rertrtert ergergergekrgkg ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke&nbsp;</p>', 3455545000000, NULL, 'active', '527db214d00e366102421d79a16d3123', '2017-12-23 06:40:07', '2017-12-23 06:40:07'),
(55, 18, 1, '1514036441.jpg', 'photo', 'ttttttt', '<p>rertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke g&nbsp; egk egk wkeg kw egk wke g rertrtert erg erg erg ekrgk g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke g sfdjhjkwhefhweetwekntt4trertrtert erg erg erg ekrg k g ejkbr ke bkj erkgwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke g ehrjrsjer 5jrtjt&nbsp;rertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egkwkeg kw egk wke g rt rtjrtu r6rertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke g 6 u6 u65 u56rertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke g&nbsp; u56 u56 u56urertrtert erg erg erg ekrg k g ejkbrkebkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke g56 u56 urertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke g56 u56 u65rertrtert ergerg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke g u56 u56u</p>\r\n<p>rertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke&nbsp;rertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg ewrgk wkejg kw egk wkeg kw egk wke grertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke grertrtert erg ergerg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke grertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgkwkejgkw egk wkeg kw egk wke grertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke grertrtert erg ergergekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke grertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejgkw egk wkeg kw egk wke grertrtert erg erg erg ekrg k g ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke g&nbsp;rertrtert ergergergekrgkg ejkbr ke bkj erk gwreg wkjrg&nbsp; ewrgk wkejg kw egk wkeg kw egk wke&nbsp;</p>', 5500000000, NULL, 'active', '527db214d00e366102421d79a16d3345', '2017-12-23 06:40:41', '2017-12-23 06:40:41'),
(56, 18, 3, '1514641358.jpg', 'photo', 'ttttttttthhhhhh', '<p>jkakakdfh</p>\r\n<p>dshsbgjbdfg</p>\r\n<p>sjbkasj</p>\r\n<p>&nbsp;</p>', 900000, NULL, 'active', '527db214d00e366102421d79a16d3890', '2017-12-30 06:42:38', '2017-12-30 06:42:38'),
(57, 18, 9, '1514712929.jpg', 'photo', '5345 456 45 645 6', '<p>jdkfksjng</p>\r\n<p>djdfgjdfnglkdfg</p>\r\n<p>fgdfnkjdnfbdnf</p>', 1000000, NULL, 'active', '05df97e4a1cde7149b0e315839b28d6b', '2017-12-31 02:35:29', '2017-12-31 02:35:29'),
(58, 18, 6, '1514713675.jpg', 'photo', 'abc hbh', '<p>akjshkjahjsf</p>\r\n<p>sdjgksjhgjsd</p>\r\n<p>gjksdkjshdjglhsjdg</p>\r\n<p>jldhjlhsldgf</p>\r\n<p>hthjlslkhjkleth</p>\r\n<p><strong>&lt;div&gt;<br />&lt;h2&gt;What is Lorem Ipsum?&lt;/h2&gt;<br />&lt;p&gt;&lt;strong&gt;Lorem Ipsum&lt;/strong&gt; is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;<br />&lt;/div&gt;</strong></p>', 1000000, NULL, 'active', '7878118ac61ccc118ca49d3db840b0cd', '2017-12-31 02:47:56', '2017-12-31 02:47:56'),
(59, 18, 6, '1514713945.jpg', 'photo', 'tutyttty', '<p>hgghg</p>', 600000, NULL, 'active', 'faa9b27d40056f9851169fb84157b41b', '2017-12-31 02:52:26', '2017-12-31 02:52:26'),
(60, 18, 1, '1520380241.jpg', 'photo', 'Khám chữa bệnh như nào', '<p>Một cơ thể khỏe mạnh, một sức khỏe dẻo dai, một cuộc sống sung t&uacute;c&hellip;l&agrave; điều mong m&otilde;i của hầu hết mỗi ch&uacute;ng ta. Nhưng! Cuộc đời d&acirc;u bể, mỗi con người một cuộc đời, một số phận v&agrave; ước muốn cũng chỉ l&agrave; ước muốn&hellip;</p>\r\n<p>Thời đại c&ocirc;ng nghệ ph&aacute;t triển, chỉ cần ngồi một nơi ch&uacute;ng ta c&oacute; thể nh&igrave;n thấy v&agrave; cảm nhận được sự thay đổi của cả thế giới, v&agrave; trong thế giới ấy c&oacute; mu&ocirc;n m&agrave;u mu&ocirc;n sắc, v&ocirc; v&agrave;n số phận con người, đ&acirc;u đ&oacute; ta vẫn thấy v&agrave;i h&igrave;nh ảnh đau đớn của sự bất lực v&agrave; tuyệt vọng trong đ&ocirc;i măt đờ đẫn v&igrave; kh&ocirc;ng may bệnh tật h&agrave;nh hạ nhưng&hellip;chi ph&iacute; điều trị qu&aacute; đắt đỏ&hellip;Chờ ph&eacute;p m&agrave;u hay &hellip;chấp nhận số phận.</p>\r\n<p>Ng&agrave;y nay với nhịp sống hối hả, m&ocirc;i trường, m&ocirc;i sinh &ocirc; nhiễm k&eacute;o theo đ&oacute; l&agrave; h&agrave;ng loạt c&aacute;c loại dịch bệnh g&acirc;y ảnh hưởng kh&ocirc;ng nhỏ đến t&acirc;m l&yacute;, cuộc sống của đa số gia đ&igrave;nh bệnh nh&acirc;n v&agrave; đặc biệt l&agrave; bệnh nh&acirc;n thuộc c&aacute;c đối tượng ngh&egrave;o, ho&agrave;n cảnh kh&oacute; khăn. Cơm ba bữa chưa lo đủ lấy đ&acirc;u chi ph&iacute; thuốc men, điều trị.</p>\r\n<p>Hầu hết bệnh nh&acirc;n thuộc c&aacute;c đối tượng n&agrave;y khi v&agrave;o viện điều trị điều trong t&igrave;nh trạng bệnh nghi&ecirc;m trọng, v&igrave; khi cơ thể c&oacute; dấu hiệu bệnh họ thường d&ugrave;ng v&agrave;i liều thuốc t&acirc;y, hoặc tự điều trị tại nh&agrave; theo phương ph&aacute;p d&acirc;n gian hoặc ai chỉ g&igrave; th&igrave; d&ugrave;ng nấy, chỉ đến khi bệnh trở nặng mới t&igrave;m đến c&aacute;c cơ sở y t&ecirc; phường, x&atilde; kiểm tra thăm kh&aacute;m.</p>\r\n<p>Mặt kh&aacute;c, chi ph&iacute; kiểm tra y tế hiện nay của ch&uacute;ng ta kh&aacute; đắt đỏ, &yacute; thức chăm s&oacute;c, kiểm tra sức khỏe định kỳ của người d&acirc;n chưa cao, đặc biệt ở c&aacute;c khu vực v&ugrave;ng s&acirc;u, v&ugrave;ng xa, khu vực miền n&uacute;i với tr&igrave;nh độ d&acirc;n tr&iacute; thấp, điều kiện kinh tế kh&oacute; khăn, cơ sở hạ tầng, vật chất c&ograve;n yếu k&eacute;m do đ&oacute; việc đến c&aacute;c trung t&acirc;m y tế hoặc bệnh viện thường gặp nhiều trở ngại.</p>\r\n<p>Người ngh&egrave;o thường quan niệm rằng, việc chăm s&oacute;c sức khỏe, sử dụng c&aacute;c dịch vụ chăm s&oacute;c y tế chỉ d&agrave;nh cho người c&oacute; thu nhập tốt, cho người gi&agrave;u m&agrave; qu&ecirc;n rằng ch&iacute;nh bản th&acirc;n m&igrave;nh mới l&agrave; đối tượng cần được quan t&acirc;m nhiều nhất, cần được chăm s&oacute;c y tế thường xuy&ecirc;n.</p>\r\n<p>Việc chăm s&oacute;c sức khỏe cho người ngh&egrave;o, người c&oacute; ho&agrave;n cảnh kh&oacute; khăn cần được quan t&acirc;m, đầu tư v&agrave; tuy&ecirc;n truyền rộng r&atilde;i nhằm n&acirc;ng cao &yacute; thức tự gi&aacute;c, phổ cập kiến thức về sức khỏe, ph&ograve;ng v&agrave; chữa bệnh trong cộng đồng d&acirc;n cư. Khuyến kh&iacute;ch người d&acirc;n sử dụng c&aacute;c dịch vụ y tế, thường xuy&ecirc;n tham gia kiểm tra sức khỏe định kỳ cho mỗi c&aacute; nh&acirc;n v&agrave; gia đ&igrave;nh.</p>', 467424974987441, NULL, 'active', '21fbe6b03e1b3c772e8c1cc585995044', '2018-03-07 06:50:41', '2018-03-07 06:50:41');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `total_campaigns` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `content`, `total_campaigns`, `created_at`, `updated_at`) VALUES
(1, 'điều trị bệnh', ' \r\n <p> Một cơ thể khỏe mạnh, một sức khỏe dẻo dai, một cuộc sống sung túc…là điều mong mõi của hầu hết mỗi chúng ta. Nhưng! Cuộc đời dâu bể, mỗi con người một cuộc đời, một số phận và ước muốn cũng chỉ là ước muốn… </p>\r\n <p>Thời đại công nghệ phát triển, chỉ cần ngồi một nơi chúng ta có thể nhìn thấy và cảm nhận được sự thay đổi của cả thế giới, và trong thế giới ấy có muôn màu muôn sắc, vô vàn số phận con người, đâu đó ta vẫn thấy vài hình ảnh đau đớn của sự bất lực và tuyệt vọng trong đôi măt đờ đẫn vì không may bệnh tật hành hạ nhưng…chi phí điều trị quá đắt đỏ…Chờ phép màu hay …chấp nhận số phận. </p>\r\n <p>Ngày nay với nhịp sống hối hả, môi trường, môi sinh ô nhiễm kéo theo đó là hàng loạt các loại dịch bệnh gây ảnh hưởng không nhỏ đến tâm lý, cuộc sống của đa số gia đình bệnh nhân và đặc biệt là bệnh nhân thuộc các đối tượng nghèo, hoàn cảnh khó khăn. Cơm ba bữa chưa lo đủ lấy đâu chi phí thuốc men, điều trị. </p>\r\n <p>Hầu hết bệnh nhân thuộc các đối tượng này khi vào viện điều trị điều trong tình trạng bệnh nghiêm trọng, vì khi cơ thể có dấu hiệu bệnh họ thường dùng vài liều thuốc tây, hoặc tự điều trị tại nhà theo phương pháp dân gian hoặc ai chỉ gì thì dùng nấy, chỉ đến khi bệnh trở nặng mới tìm đến các cơ sở y tê phường, xã kiểm tra thăm khám. </p>\r\n <p>Mặt khác, chi phí kiểm tra y tế hiện nay của chúng ta khá đắt đỏ, ý thức chăm sóc, kiểm tra sức khỏe định kỳ của người dân chưa cao, đặc biệt ở các khu vực vùng sâu, vùng xa, khu vực miền núi với trình độ dân trí thấp, điều kiện kinh tế khó khăn, cơ sở hạ tầng, vật chất còn yếu kém do đó việc đến các trung tâm y tế hoặc bệnh viện thường gặp nhiều trở ngại.</p>\r\n <p>Người nghèo thường quan niệm rằng, việc chăm sóc sức khỏe, sử dụng các dịch vụ chăm sóc y tế chỉ dành cho người có thu nhập tốt, cho người giàu mà quên rằng chính bản thân mình mới là đối tượng cần được quan tâm nhiều nhất, cần được chăm sóc y tế thường xuyên.</p>\r\n <p>Việc chăm sóc sức khỏe cho người nghèo, người có hoàn cảnh khó khăn cần được quan tâm, đầu tư và tuyên truyền rộng rãi nhằm nâng cao ý thức tự giác, phổ cập kiến thức về sức khỏe, phòng và chữa bệnh trong cộng đồng dân cư. Khuyến khích người dân sử dụng các dịch vụ y tế, thường xuyên tham gia kiểm tra sức khỏe định kỳ cho mỗi cá nhân và gia đình. \r\n</p>\r\n', 7, NULL, '2018-03-07 06:50:41'),
(2, 'khẩn cấp', '<p><span style="font-weight: 400;">Cuộc sống l&agrave; một bức tranh lớn m&agrave; mỗi người ch&uacute;ng ta l&agrave; 1 mảnh gh&eacute;p. Mỗi mảnh gh&eacute;p c&oacute; những ho&agrave;n cảnh, số phận đ&aacute;ng thương kh&aacute;c nhau m&agrave; ch&uacute;ng ta kh&ocirc;ng ai mong muốn. Đừng nghĩ cuộc sống l&agrave; một mặt nước phẳng lặng, lu&ocirc;n c&oacute; những đợt sống ngầm k&eacute;o đến m&agrave; ch&uacute;ng ta kh&ocirc;ng thể trở tay kịp. Đ&oacute; l&agrave; những trường hợp khẩn cấp cần c&oacute; sự gi&uacute;p đỡ từ nhiều người. Cũng giống như &ocirc;ng b&agrave; ta vẫn n&oacute;i: &ldquo;L&aacute; l&agrave;nh đ&ugrave;m l&aacute; r&aacute;ch&rdquo; l&agrave; thế. </span></p>\r\n<p><span style="font-weight: 400;">Nhắc đến những trường hợp khẩn cấp trong cuộc sống, ch&uacute;ng ta kh&ocirc;ng thể đếm hết: thi&ecirc;n tai, hỏa hoạn, cướp giật,&hellip; l&agrave; những t&igrave;nh huống bất ngờ xảy đến kh&ocirc;ng ai lường trước được. V&igrave; thế, trong những trường hợp khẩn cấp ấy rất cần c&oacute; những tổ chức, c&aacute; nh&acirc;n hảo t&acirc;m chung tay gi&uacute;p đỡ họ.</span></p>\r\n<p><span style="font-weight: 400;">L&agrave; một tổ chức mang t&iacute;nh cộng đồng, với mong muốn kết nối, gi&uacute;p đỡ những trường hợp khẩn cấp trong cuộc sống, ch&uacute;ng t&ocirc;i &ndash; những bạn trẻ nhiệt huyết v&agrave; tr&aacute;i tim y&ecirc;u thương sẽ l&agrave; những người cung cấp th&ocirc;ng tin ch&iacute;nh x&aacute;c c&aacute;c trường hợp ấy v&agrave; chuyển tải đến c&aacute;c c&aacute; nh&acirc;n, tổ chức c&oacute; tấm l&ograve;ng hảo t&acirc;m để họ c&oacute; thể gi&uacute;p đỡ những ho&agrave;n cảnh ấy, thể hiện một phần tinh thần tương th&acirc;n tương &aacute;i của d&acirc;n tộc Việt Nam. </span></p>\r\n<p><span style="font-weight: 400;">C&aacute;c tổ chức, c&aacute; nh&acirc;n sẽ ho&agrave;n to&agrave;n y&ecirc;n t&acirc;m với việc đảm bảo th&ocirc;ng tin v&agrave; được gửi trao tận tay những hiện vật của m&igrave;nh cho c&aacute;c trường hợp cần gi&uacute;p đỡ ấy m&agrave; kh&ocirc;ng phải th&ocirc;ng qua trung gian. </span></p>\r\n<p><span style="font-weight: 400;">Những việc tuy nhỏ nhưng kịp thời sẽ mang đến một tương lai kh&aacute;c biệt ho&agrave;n to&agrave;n cho những ho&agrave;n cảnh khẩn cấp ấy. </span></p>\r\n<h3 id="intl"><br /><br /><br /><br /></h3>', 10, NULL, '2018-02-27 19:42:44'),
(3, 'giáo dục', ' \r\n <p> “Giáo dục lá quốc sách hàng đầu”, từ xưa đến nay Đảng và Nhà nước ta luôn đề cao tầm quan trọng của giáo dục. Không có chiếc chìa khoá nào mở cánh cửa tương lai nhanh chóng bằng việc tiếp thu tri thức. Tri thức cũng chính là khối tài sản vô giá mà mỗi con người chúng ta có được trong hành trình sống của mình. </p>\r\n\r\n <p> Giáo dục - đào tạo không chỉ có vai trò quan trọng trên lĩnh vực sản xuất vật chất mà còn là cơ sở để hình thành nền văn hoá tinh thần của chủ nghĩa xã hội. Một nền giáo dục kém sẽ ươm mầm những hạt giống kém chất lượng và không làm nên những cây “đại thụ” vững chắc để tạo dựng nên nền móng kinh tế - xã hội phát triển. Vì thế, mục tiêu của nước ta là tất cả trẻ em trong độ tuổi đi học đều được đến trường. Mặc dù đã nổ lực giảm thiểu số lượng trẻ em mù chữ ở nước ta nhưng Việt Nam hiện nay vẫn còn khảoảng 3.5 triệu trong tổng số 23 triệu trẻ em các cấp bỏ học và nhiểu em khác không có điều kiện đến trường. Vậy làm thế nào để tất cả trẻ em không có điều kiện ấy tiếp cận được với tri thức mà không bỏ sót một ai?\r\n</p>\r\n <p> Chúng tôi sẽ là những người cho bạn câu trả lời. Bằng tâm huyết, sức trẻ và tinh yêu thương, chúng tôi sẽ là tạo điều kiện, giúp đỡ những trẻ em không có cơ hội đến trường được tiếp cận tri thức như những bạn bè đồng trang lứa khác. Ngoài ra, với những cơ quan trường học có nhu cần giúp đỡ cho việc giảng dạy cũng sẽ được chúng tôi kết nối với các nhà hảo tâm một cách nhanh chóng, chính xác. Những giáo viên tâm huyết với nghề có hoàn cảnh khó khăn cần được giúp đỡ cũng sẽ được chúng tôi thông tin đến các nhà thiện nguyện, kết nối và sẻ chia để ngọn lửa nghề của họ có thể tiếp tục cháy hơn nữa vì thế hệ trẻ tương lai. \r\n</p>', 9, NULL, '2017-12-30 06:42:38'),
(4, 'động vật', ' \r\n <p> “Sống trong đời sống cần có một tấm lòng…”\r\nCâu hát quen thuộc ấy vẫn hiện diện trong tâm thức mỗi người như một lời nhắc nhở về sự sẻ chia, yêu thương. Đó không chỉ là tình yêu giữa người với người mà còn là một thứ tình cảm đặc biệt nhưng cũng không kém phần ấm áp giữa con người và loài vật.</p>\r\n<p>Trái đất là mái nhà chung giữa con người và trong Phật giáo cũng có câu: Vạn vật hữu linh. Loài vật cũng như con người, đều có tâm hồn. Sự sẻ chia giữa con người và loài vật cũng là một thứ tình yêu thương kỳ diệu và Thế giới sẽ thanh bình biết bao nếu tất cả cùng chung sống hoà thuận. </p>\r\n<p>Đó là lí do vì sao ở Việt Nam cũng như trên Thế giới luôn có nhiều vật nuôi được sống chung một mái nhà với con người, và rất nhiều hội, nhóm yêu động vật cũng như hội bảo vệ động vật hoang dã có mặt trên cả nước. Ngoài việc chăm sóc những con vật nuôi hằng ngày, chúng ta có thể giúp đỡ các tổ chức yêu động vật các điều kiện cần thiết để họ xây dựng môi trường sống cho động vật. Đó là việc làm vô cùng thiết thực.</p>\r\n<p>Nếu bạn cũng có chung tình yêu thương dành cho động vật và mong muốn bảo vệ chúng và cũng là bảo vệ trái đất này, hãy tìm đến chúng tôi.  </p>\r\n<p>Sứ mệnh của chúng tôi là gắn kết yêu thương từ động vật cho đến con người ở khắp mọi miền. Chỉ cần bạn có tấm lòng, chúng tôi sẽ có giải pháp. Không quá khó khăn và phức tạp trong việc đi tìm chủ nhân hay một nhà hảo tâm nào đó cưu mang một con vật đang đi hoang hay một đơn vị/ tổ chức cần xây dựng sở thú chưa đủ kinh phí, hoặc đơn giản bạn chỉ muốn nhận nuôi một con vật đáng yêu nào đấy để có thể làm bạn mỗi ngày và giúp một động vật nào đó về lại môi trường sống vỗn dĩ thuộc về nó… Chúng tôi sẽ là những người xây dựng cầu nối cho bạn và những tấm lòng thiện nguyện có điều kiện khác để cùng nhau kiến tạo nên một Thế giới tràn ngập yêu thương – hoà bình – nhân ái có sự liên kết chặt chẽ giữa con người và loài vật. \r\n\r\n</p>', 5, NULL, '2017-12-12 09:51:03'),
(5, 'thể thao', '<p><span style="font-weight: 400;">Trải qua nhiều năm h&igrave;nh th&agrave;nh v&agrave; ph&aacute;t triển, thể dục thể thao lu&ocirc;n giữ vai tr&ograve; quan trọng trong việc n&acirc;ng cao sức khỏe v&agrave; chất lượng cuộc sống con người. Cuộc sống ng&agrave;y một hiện đại th&igrave; thể thao ng&agrave;y đươc xem trọng. Đ&oacute; kh&ocirc;ng chỉ l&agrave; tho&iacute; quen để r&egrave;n luyện cơ thể mỗi ng&agrave;y m&agrave; c&ograve;n l&agrave; một m&ocirc;n &ndash; một tr&ograve; chơi để thể hiện sức mạnh, đẳng cấp, kỹ thuật của nuớc nh&agrave; khi vươn ra &nbsp;biển lớn&rdquo; c&ugrave;ng nước bạn. </span></p>\r\n<p><span style="font-weight: 400;">Chủ tịch Hồ Ch&iacute; Minh cũng nhận định: "Thể dục thể thao l&agrave; một c&ocirc;ng t&aacute;c c&aacute;ch mạng", tức l&agrave; Người đ&atilde; đặt thể dục thể thao ngang h&agrave;ng với c&aacute;c c&ocirc;ng t&aacute;c kh&aacute;c, như c&ocirc;ng t&aacute;c ch&iacute;nh trị tư tưởng, c&ocirc;ng t&aacute;c tổ chức, c&ocirc;ng t&aacute;c văn ho&aacute;, gi&aacute;o dục... C&ocirc;ng t&aacute;c thể dục thể thao c&oacute; nhiệm vụ "nghi&ecirc;n cứu phương ph&aacute;p v&agrave; thực h&agrave;nh thể dục trong to&agrave;n quốc" nhằm "tăng bổ sức khoẻ quốc d&acirc;n v&agrave; cải tạo n&ograve;i giống Việt Nam".</span></p>\r\n<p><span style="font-weight: 400;">Ng&agrave;y nay, c&ugrave;ng với sự ph&aacute;t triển kinh tế - x&atilde; hội, nhiều m&ocirc;n thể thao dần được chuy&ecirc;n nghiệp h&oacute;a; tiếp cận với phương ph&aacute;p tập luyện, thi đấu hiện đại v&agrave; tr&igrave;nh độ khu vực kh&aacute;c nhau. Kh&ocirc;ng &iacute;t m&ocirc;n thể thao c&oacute; t&iacute;nh truyền thống đ&atilde; ph&aacute;t triển mạnh v&agrave; rộng, trở th&agrave;nh m&ocirc;n thể thao được y&ecirc;u th&iacute;ch, được đưa v&agrave;o thi đấu ở c&aacute;c nước, trong c&aacute;c giải khu vực v&agrave; thế giới.</span></p>\r\n<p><span style="font-weight: 400;">Thể thao kh&ocirc;ng chỉ r&egrave;n luyện thể lực m&agrave; c&ograve;n l&agrave; kết tinh cho tinh thần đồng đội, sức mạnh tập thể v&agrave; tu dưỡng đạo đức th&ocirc;ng qua qu&aacute; tr&igrave;nh tập luyện. Tuy nhi&ecirc;n, kh&ocirc;ng phải ai cũng c&oacute; đủ điều kiện về thể chất lẫn vật chất để theo đuổi m&ocirc;n thể thao m&agrave; m&igrave;nh y&ecirc;u th&iacute;ch. Với mong muốn l&agrave; chiếc cầu nối cho tất cả những ai y&ecirc;u thể thao được theo đuổi đam m&ecirc;, cống hiến sức trẻ để trở th&agrave;nh những chiến binh, những người hung cho nước nh&agrave;, ch&uacute;ng t&ocirc;i sẽ cung cấp cho bạn th&ocirc;ng tin về những trường hợp cần gi&uacute;p đỡ, kết nối với c&aacute;c nh&agrave; hảo t&acirc;m để gi&uacute;p đỡ c&aacute;c c&aacute; nh&acirc;n y&ecirc;u thể thao hay những cơ quan, tổ chức muốn ph&aacute;t triển lĩnh vực thể dục &ndash; thể thao nhưng vẫn c&ograve;n yếu k&eacute;m. </span></p>\r\n<p><span style="font-weight: 400;">Đ&oacute; ch&iacute;nh l&agrave; sứ mệnh của ch&uacute;ng t&ocirc;i &ndash; Kết nối v&agrave; Kết nối. </span></p>\r\n<p><br /><br /><br /></p>', 6, NULL, '2018-02-27 19:43:10'),
(6, 'Ước Mơ', '<p><span style="font-weight: 400;">Bạn c&oacute; tin kh&ocirc;ng? &ldquo;</span><em><span style="font-weight: 400;">Ước mơ giống như lo&agrave;i chim, cảm nhận buổi ban mai v&agrave; khẽ kh&agrave;ng cất tiếng h&oacute;t khi trời vẫn c&ograve;n tối&rdquo;. </span></em><span style="font-weight: 400;">Trong cuộc sống, ai cũng ấp ủ cho m&igrave;nh một ước mơ ri&ecirc;ng. Bởi ước mơ kh&ocirc;ng đ&aacute;nh thuế bao giờ, thế n&ecirc;n cứ mơ ước thật lớn v&agrave; đừng sợ h&atilde;i.</span></p>\r\n<p><span style="font-weight: 400;">Những người c&oacute; ước mơ l&agrave; những người đ&atilde; nắm được 50% phần thắng trong cuộc sống. Uớc mơ được đến lớp như bao bạn b&egrave; của c&ocirc; b&eacute; khuyết tật, l&agrave; nỗi kh&aacute;t khao được c&oacute; m&aacute;i ấm gia đ&igrave;nh để nương tựa, l&agrave; giấc mơ được theo đuổi con đường trở th&agrave;nh một hoạ sĩ, b&aacute;c sĩ,&hellip; Thế nhưng kh&ocirc;ng phải ước mơ n&agrave;o cũng sẽ th&agrave;nh hiện thực. Đ&acirc;u đ&oacute; trong cuộc sống vẫn c&ograve;n những ước mơ như ngọn nến, vừa chớm le l&oacute;i đ&atilde; vội tắt v&igrave; những l&iacute; do ri&ecirc;ng. C&oacute; người kh&ocirc;ng đủ điều kiện kinh tế, gia đ&igrave;nh, cũng c&oacute; người v&igrave; cơ thể bị khiếm khuyết, mặc cảm, t&acirc;m l&yacute;,&hellip;m&agrave; ước mơ bị dang dở. &nbsp;Đ&oacute; l&agrave; những r&agrave;o cản cho mọi đ&iacute;ch để chinh phục th&agrave;nh c&ocirc;ng v&agrave; hạnh ph&uacute;c. Người ta vẫn bảo &ldquo;Mỗi c&acirc;y mỗi hoa, mỗi nh&agrave; mỗi cảnh&rdquo;, ch&uacute;ng ta kh&ocirc;ng thể lựa chọn một điều kiện sống tốt nhưng ch&uacute;ng ta c&oacute; quyền ước mơ. Đừng để ước mơ bị ch&ugrave;n bước chỉ v&igrave; những điều kiện kh&aacute;ch quan ấy. </span></p>\r\n<p><span style="font-weight: 400;">Nếu bạn đang ấp ủ những ước mơ m&agrave; vẫn chưa c&oacute; điều kiện để hiện thực ho&aacute; ước mơ ấy, h&atilde;y nghĩ đến ch&uacute;ng t&ocirc;i. Những người trẻ, đầy nhiệt huyết v&agrave; t&igrave;nh y&ecirc;u sẽ kết nối bạn với những tấm l&ograve;ng thiện nguyện để bạn gi&uacute;p đỡ bạn chạm đến ước mơ của m&igrave;nh một c&aacute;ch nhanh ch&oacute;ng v&agrave; dễ d&agrave;ng nhất, &nbsp;</span></p>\r\n<p><span style="font-weight: 400;">Ch&uacute;ng t&ocirc;i sẽ l&agrave; những đại sứ ước mơ tin cậy nhất gi&uacute;p bạn c&oacute; th&ecirc;m đ&ocirc;i c&aacute;nh bay đến nấc thang cuối c&ugrave;ng rực rỡ nhất trong cuộc sống của m&igrave;nh. Nhiệm vụ của ch&uacute;ng t&ocirc;i l&agrave; kh&ocirc;ng bỏ s&oacute;t những ai c&oacute; ước mơ v&agrave; lu&ocirc;n nổ lực thực hiện điều đ&oacute;. Điều m&agrave; ch&uacute;ng t&ocirc;i c&oacute; thể mang đến cho mọi người đ&oacute; ch&iacute;nh l&agrave; t&igrave;m ra những người c&oacute; ước mơ ch&acirc;n ch&iacute;nh v&agrave; kết nối, gi&uacute;p đỡ họ thực hiện ước mơ ấy để cảm thấy &ldquo;Cuộc đời vẫn đẹp sao!&rdquo;</span></p>\r\n<h2><br /><br /></h2>', 8, NULL, '2018-02-27 19:44:38'),
(7, 'tổ chức từ thiện', '<div>&nbsp;</div>\r\n<p>Where does it come from?</p>\r\n<div>\r\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>\r\n</div>', 7, NULL, '2017-12-23 06:40:07'),
(8, 'tín ngưỡng', '<p><span style="font-weight: 400;">Trong cuộc sống, c&oacute; đ&ocirc;i l&uacute;c bạn sẽ cảm thấy mệt mỏi, suy sụp tinh thần v&igrave; những chuyện g&igrave; đ&oacute;. Một khi đ&atilde; trở n&ecirc;n bất lực, con người ta lại mong muốn t&igrave;m đến một nơi nương tựa, một chỗ dựa tinh thần v&ocirc; h&igrave;nh nhưng vững chắc. Đ&oacute; l&agrave; đời sống t&iacute;n ngưỡng, t&ocirc;n gi&aacute;o. </span></p>\r\n<p><span style="font-weight: 400;">T&iacute;n ngưỡng, t&ocirc;n gi&aacute;o l&agrave; một phần kh&ocirc;ng thể thiếu trong đời sống người Việt. </span><span style="font-weight: 400;">Lịch sử cho thấy rằng c&aacute;c loại t&iacute;n ngưỡng kh&aacute;c nhau c&oacute; sức sống dai dẳng v&agrave; ảnh hưởng l&acirc;u d&agrave;i m&agrave; kh&oacute; c&oacute; hệ tư tưởng n&agrave;o s&aacute;nh được. Từ việc thờ c&uacute;ng &ocirc;ng b&agrave; tổ ti&ecirc;n, đến việc c&aacute;c ch&ugrave;a chiềng, nh&agrave; thờ được x&acirc;y dựng khắp nơi l&agrave;m chỗ dựa t&acirc;m linh cho con người, đ&atilde; cho thấy tầm quan trọng của t&iacute;n ngưỡng, t&ocirc;n gi&aacute;o đ&atilde; ăn s&acirc;u v&agrave;o tiềm thức, đời sống người Việt, l&agrave; một trong những th&agrave;nh phần tạo n&ecirc;n bản sắc văn h&oacute;a của c&aacute;c d&acirc;n tộc. Những triết l&yacute; sống m&agrave; t&iacute;n ngưỡng, t&ocirc;n gi&aacute;o mang đến cho người Việt kh&ocirc;ng chỉ g&oacute;i gọn trong việc thể hiện vai tr&ograve; của một trụ cột tinh thần m&agrave; đ&oacute; c&ograve;n l&agrave; những b&agrave;i học sống thờ cha k&iacute;nh mẹ, l&ograve;ng biết ơn tổ nghiệp, thần ho&agrave;ng,&hellip;</span></p>\r\n<p><span style="font-weight: 400;">Tuy nhi&ecirc;n, để t&iacute;n ngưỡng, t&ocirc;n gi&aacute;o được bền vững theo thời gian v&agrave; c&oacute; cơ sở ph&aacute;t triển hơn nữa trong cuộc sống, cần lắm những c&aacute; nh&acirc;n, đơn vị - tổ chức thiện nguyện chung tay gi&uacute;p đỡ. Bởi trong x&atilde; hội vẫn c&ograve;n rất nhiều ng&ocirc;i ch&ugrave;a chưa khang trang, nhiều t&iacute;n đồ Phật gi&aacute;o, Thi&ecirc;n ch&uacute;a gi&aacute;o vẫn c&ograve;n gặp nhiều kh&oacute; khăn. C&ugrave;ng nhau t&igrave;m thấy v&agrave; kết nối gi&uacute;p đỡ họ cũng l&agrave; gi&uacute;p đỡ nền t&ocirc;n gi&aacute;o, t&iacute;n ngưỡng Việt bền vững hơn. Đồng thời, c&ugrave;ng nhau k&ecirc;u gọi gi&uacute;p đỡ c&aacute;c hoạt động tuy&ecirc;n truyền đạo l&yacute; sống tốt, &yacute; nghĩa, cho những người k&eacute;m may mắn trong x&atilde; hội c&ugrave;ng c&aacute;c tổ chức t&ocirc;n gi&aacute;o, t&iacute;n ngưỡng cũng l&agrave; c&aacute;ch để x&acirc;y dựng.</span></p>\r\n<p><span style="font-weight: 400;">Nếu bạn cần những đại sứ t&ocirc;n gi&aacute;o như thế, h&atilde;y li&ecirc;n hệ với ch&uacute;ng t&ocirc;i. Bạn sẽ t&igrave;m thấy mối tương đồng v&agrave; sự tin cậy ở ch&uacute;ng t&ocirc;i. Ch&uacute;ng t&ocirc;i sẽ kết nối, k&ecirc;u gọi gi&uacute;p đỡ c&aacute;c hội, nh&oacute;m li&ecirc;n quan đến t&iacute;n ngưỡng - t&ocirc;n gi&aacute;o c&oacute; mục đ&iacute;ch &yacute; nghĩa, thiết thực cho x&atilde; hội.</span><br /><br /></p>', 3, NULL, '2018-02-27 19:45:45'),
(9, 'Cộng Đồng', '<h3 id="intl">dfgdfgdfgdgdfgd</h3>\r\n<p>fgdfgdfhdfhd</p>\r\n<p>fhdfh</p>\r\n<p>dhf</p>\r\n<p>dhf</p>\r\n<p>dfhdfhjdfjdfj</p>', 5, NULL, '2018-02-27 19:40:13');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE IF NOT EXISTS `contact_us` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `full_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=44 ;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `full_name`, `email`, `phone_number`, `message`, `created_at`, `updated_at`) VALUES
(2, 'minh2', 'minhnguyen.admin@yopmail.com', '534634763', 'sfnsnsnfxbzxxfn dfksdfsd sgdfdfhjsdghs sdjghshgskdv sdgshhieughegb sdgsheghsdg sdghskdhgskjdhgjsdhg sdjghsehgshghkeg sgksjehgkshgkjsehg segjkshegheskjghkes gsjkeghskhgskeg', '2017-12-14 09:51:03', '2017-12-14 09:51:03'),
(3, 'minh3', 'minh3@yopmail.com', '235235235235', 'dssdbsdbd', '2017-12-14 09:51:32', '2017-12-14 09:51:32'),
(4, 'minh4', 'minh4@yopmail.com', '34634636', 'sfndnfndndfn', '2017-12-14 09:51:49', '2017-12-14 09:51:49'),
(5, 'minh5', 'minh5@yopmail.com', '346346346346', 'c cv cv', '2017-12-14 09:52:06', '2017-12-14 09:52:06'),
(6, 'minh6', 'minh6@yopmail.com', '34636363', 'edjnffgmgm', '2017-12-14 09:52:20', '2017-12-14 09:52:20'),
(7, 'minh7', 'minh7@yopmail.com', '4634636346', 'dgndgnfnfgnfmgfggm', '2017-12-14 09:52:35', '2017-12-14 09:52:35'),
(8, 'minh8', 'minh8@yopmail.com', '856564', '6854khgkghkgh', '2017-12-14 09:52:55', '2017-12-14 09:52:55'),
(9, 'minh9', 'minh9@yopmail.com', '34634636', '34hhghfg', '2017-12-14 09:53:12', '2017-12-14 09:53:12'),
(10, 'minh10', 'minh10@yopmail.com', '457457457', 'gmfgmfgmfgmfm', '2017-12-14 09:53:28', '2017-12-14 09:53:28'),
(11, 'minh11', 'minh11@yopmail.com', '7457457', '4574574457475', '2017-12-14 09:53:51', '2017-12-14 09:53:51'),
(12, 'minh12', 'minh12@yopmail.com', '323426456', 'dgnfnfgmfgm', '2017-12-14 09:54:05', '2017-12-14 09:54:05'),
(13, 'minh13', 'minh13@yopmail.com', '5457457', 'hfgngngmfhm', '2017-12-14 09:54:21', '2017-12-14 09:54:21'),
(14, 'minh14', 'minh14@yopmail.com', '36346346', '346346346346', '2017-12-14 09:54:35', '2017-12-14 09:54:35'),
(15, 'Edwardunset', 'olgadanilenko@bk.ru', '86164173529', '[ОГРАНИЧЕННОЕ ПРЕДЛОЖЕНИЕ -  Зарабатывай в сети от 14625 руб. за сутки.: http://googlenoomon.info/r/08/?p=7020', '2018-03-05 22:30:17', '2018-03-05 22:30:17'),
(16, 'RichardBug', 'queirona@list.ru', '88159236444', '[ЭКСКЛЮЗИВНЫЙ КУРС -  Заработок в интернете от 15794 руб. в сутки.: http://googlenoomon.info/rus/?p=62518', '2018-03-09 16:33:51', '2018-03-09 16:33:51'),
(17, 'Danteevese', 'farkeeti@hotmail.com', '86924469173', 'How can I make $1,200 a month online by working part-time for 12 hours weekly?\r\n: http://usa.googlenoomon.info/?p=18475', '2018-03-16 03:26:59', '2018-03-16 03:26:59'),
(18, 'Danteevese', 'cema2320@aol.com', '87939966259', 'The ultimate way to earn from $ 3000 each day, even without your site\r\n: http://usa-money.googlenoomon.info/?p=45551', '2018-03-24 21:30:36', '2018-03-24 21:30:36'),
(19, 'Danteevese', 'nancyh2055@yahoo.com', '89171976956', 'Per day for lazy persons how do I earn from $ 3000\r\n: http://usa.googlenoomon.info/?p=62845', '2018-03-25 21:12:48', '2018-03-25 21:12:48'),
(20, 'Ernestosal', 'q@mail.gmail.com', '84862942117', 'Good afternoon \r\nIf you are thinking about our offer, the facts are here: http://m.googlegoldpay.info/?p=11047 \r\nOur present is valid till April 30, 2018', '2018-03-31 19:40:34', '2018-03-31 19:40:34'),
(21, 'Ernestosal', 'teixidor@hotmail.com', '85365155779', 'Good morning \r\nIf you are thinking about our offer, the facts are here: http://money.googlegoldpay.info/?p=35597 \r\nOur offer is valid till April 30, 2018', '2018-04-01 00:06:00', '2018-04-01 00:06:00'),
(22, 'Ernestosal', 'jmoriele@hotmail.com', '81214191329', 'Good morning \r\nIf you are thinking about our offer, the details are here: http://en.googlegoldpay.info/?p=28722 \r\nOur present is valid till April 30, 2018', '2018-04-01 04:42:28', '2018-04-01 04:42:28'),
(23, 'Ernestosal', 'moneygram517@gmail.com', '88879595885', 'Hello \r\nIf you are thinking about our offer, the details are here: http://en.googlegoldpay.info/?p=30641 \r\nOur offer is valid till April 30, 2018', '2018-04-01 14:05:19', '2018-04-01 14:05:19'),
(24, 'Ernestosal', 'natcartier@yahoo.com', '85859997889', 'Good evening \r\nIf you are thinking about our offer, the details are here: http://m.googlegoldpay.info/?p=45829 \r\nOur present is valid till April 30, 2018', '2018-04-01 18:55:56', '2018-04-01 18:55:56'),
(25, 'Ernestosal', 'minosave@yahoo.com', '81629252559', 'Good evening \r\nIf you are thinking about our offer, the facts are here: http://m.googlegoldpay.info/?p=16392 \r\nOur present is valid till April 30, 2018', '2018-04-01 23:43:26', '2018-04-01 23:43:26'),
(26, 'Ernestosal', 'midknight_2@msn.com', '83636238754', 'Good morning \r\nIf you are thinking about our offer, the facts are here: http://money.googlegoldpay.info/?p=35778 \r\nOur present is valid till April 30, 2018', '2018-04-02 04:46:02', '2018-04-02 04:46:02'),
(27, 'Ernestosal', 'chamanti@hotmail.com', '84524613585', 'Good evening \r\nIf you are interested in our offer, the facts are here: http://m.googlegoldpay.info/?p=40139 \r\nOur give is valid till April 30, 2018', '2018-04-02 09:19:48', '2018-04-02 09:19:48'),
(28, 'AlbertoExent', 'arcalia@yandex.ru', '85761891248', 'Все подробности здесь: http://ru.googlegoldpay.info/?p=51840', '2018-04-03 04:55:37', '2018-04-03 04:55:37'),
(29, 'AlbertoExent', 'nata908@inbox.ru', '88768159686', 'Вам сюда: http://vw.googlegoldpay.info/?p=12600', '2018-04-03 09:45:24', '2018-04-03 09:45:24'),
(30, 'AlbertoExent', 'andres.07@mail.ru', '84767167439', 'Здесь вся инвормация: http://vw.googlegoldpay.info/?p=1917', '2018-04-03 14:34:32', '2018-04-03 14:34:32'),
(31, 'AlbertoExent', 'mkrasuckiy@mail.ru', '82654388782', 'Если хочешь денег, тебе сюда: http://wm.googlegoldpay.info/?p=45436', '2018-04-03 20:09:57', '2018-04-03 20:09:57'),
(32, 'AlbertoExent', 'oxis1988@mail.ru', '89814829893', 'Заходи и смотри как же столько заработать: http://wm.googlegoldpay.info/?p=19686', '2018-04-04 01:09:45', '2018-04-04 01:09:45'),
(33, 'AlbertoExent', 'progress_ive7@mail.ru', '88587688683', 'Узнай быстрей как...: http://vw.googlegoldpay.info/?p=25773', '2018-04-04 05:59:21', '2018-04-04 05:59:21'),
(34, 'AlbertoExent', 'thhammond@msn.com', '87387718674', '6 effective ways to quickly earn easy money you can download this website link in PDF format: http://oop.googlenoomon.info/?p=50086 \r\nI work for each of these methods and earn much more than $ 35,000 per month.', '2018-04-04 21:47:12', '2018-04-04 21:47:12'),
(35, 'RobertAwade', 'erdi7@hotmail.com', '86757178547', '6 effective ways to quickly earn easy money you can download this link in PDF format: http://m1.googlenoomon.info/?p=6512 \r\nI work for every of the aforementioned methods and earn much more than $ 35,000 per month.', '2018-04-05 06:43:13', '2018-04-05 06:43:13'),
(36, 'RobertAwade', 'mickeyln@hotmail.com', '85172178621', '6 effective ways to quickly earn easy money you can download this hyperlink in PDF format: http://piu.googlenoomon.info/?p=25218 \r\nI work for each and every of the above mentioned methods and earn much more than $ 35,000 monthly.', '2018-04-05 18:11:49', '2018-04-05 18:11:49'),
(37, 'RobertAwade', 'roshir15@hotmail.com', '89824516692', '6 effective ways to quickly earn easy money you can download this link in PDF format: http://ready.googlenoomon.info/?p=38326 \r\nI work for every of the above mentioned methods and earn more than $ 35,000 monthly.', '2018-04-06 09:45:23', '2018-04-06 09:45:23'),
(38, 'Richarddruth', 'btrov@hotmail.com', '89349581189', 'How to start from scratch and build a big money business in no time flat? \r\nHow to make money even if you''ve never made a penny online before? \r\nHow to create an EXCITING INCOME with a few simple techniques? \r\nand more... \r\n \r\nhttp://pagebin.com/ieCysGth \r\n \r\nTap into this 70 BILLION dollar traffic source for FREE? \r\nGet a piece of this 70 billion dollar traffic juggernaught? \r\nHi, \r\nWhat if you could tap into the worlds largest FREE traffic source on earth, and put YOUR ADS in front of it? \r\nHow much traffic would you want? \r\n==> click here to get traffic ==>  http://pagebin.com/ieCysGth', '2018-04-06 19:36:41', '2018-04-06 19:36:41'),
(39, 'Richarddruth', 'ellea5@hotmail.com', '89734732256', 'How to start from scratch and build a big money business in no time flat? \r\nHow to make money even if you''ve never made a penny online before? \r\nHow to create an EXCITING INCOME with a few simple techniques? \r\nand more... \r\n \r\nhttp://pagebin.com/ieCysGth \r\n \r\nTap into this 70 BILLION dollar traffic source for FREE? \r\nGet a piece of this 70 billion dollar traffic juggernaught? \r\nHi, \r\nWhat if you could tap into the worlds largest FREE traffic source on earth, and put YOUR ADS in front of it? \r\nHow much traffic would you want? \r\n==> click here to get traffic ==>  http://pagebin.com/ieCysGth', '2018-04-07 04:55:47', '2018-04-07 04:55:47'),
(40, 'Ramonmah', 'batgu@hotmail.com', '81781286797', 'How to start from scratch and build a big money business in no time flat? \r\nHow to make money even if you''ve never made a penny online before? \r\nHow to create an EXCITING INCOME with a few simple techniques? \r\nand more... \r\n \r\nhttp://2500-per-day.googlegoldpay.info/?p=38255 \r\n \r\nTap into this 70 BILLION dollar traffic source for FREE? \r\nGet a piece of this 70 billion dollar traffic juggernaught? \r\nHi, \r\nWhat if you could tap into the worlds largest FREE traffic source on earth, and put YOUR ADS in front of it? \r\nHow much traffic would you want? \r\n==> click here to get traffic ==> http://2500-per-day.googlegoldpay.info/?p=33616', '2018-04-09 04:38:28', '2018-04-09 04:38:28'),
(41, 'Mariofub', 'wwf35@hotmail.com', '87532139765', 'Hi, \r\n \r\nDid you check out this secret system yet? \r\n \r\nThe one that ordinary people are using to make up to $2,000 each day. \r\n \r\nAll you need to do is follow the simple instructions and you can earn $10k within the next 5 days \r\n \r\nClick here now to signup http://2000today.googlegoldpay.info/?p=31135 \r\n \r\nDo NOT miss the opportunity to start banking easy online cash today! \r\n \r\nClick here to sign up \r\nhttp://2000.googlegoldpay.info/?p=31230', '2018-04-10 10:00:10', '2018-04-10 10:00:10'),
(42, 'Mariofub', 'babybebe@msn.com', '81255345522', 'Hi, \r\n \r\nDid you check out this secret system yet? \r\n \r\nThe one that ordinary people are using to make up to $2,000 each day. \r\n \r\nAll you need to do is follow the simple instructions and you can earn $10k within the next 5 days \r\n \r\nClick here now to signup http://2000today.googlegoldpay.info/?p=18922 \r\n \r\nDo NOT miss the opportunity to start banking easy online cash today! \r\n \r\nClick here to sign up \r\nhttp://2000.googlegoldpay.info/?p=35297', '2018-04-10 20:19:10', '2018-04-10 20:19:10'),
(43, 'ReggieBuiva', 'williamcohen@msn.com', '84627441826', 'Extremely good news. \r\n \r\nI downloaded this yesterday and it made me $2,421.28 \r\n \r\nThese guys GUARANTEE that it will make you $2,000 within 24 hours. \r\n \r\nDO NOT miss out on this. \r\n \r\nPlease follow the link below to claim your cash by midnight today… \r\n \r\n=> Claim your $2,000 here today http://bestusamakemoney.tk/?p=7365 \r\n \r\nCongratulations!', '2018-04-13 19:50:11', '2018-04-13 19:50:11');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `position` int(11) DEFAULT NULL,
  `menu_type` int(11) NOT NULL DEFAULT '1',
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `position`, `menu_type`, `icon`, `name`, `title`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, NULL, 0, NULL, 'User', 'User', NULL, NULL, NULL),
(2, NULL, 0, NULL, 'Role', 'Role', NULL, NULL, NULL),
(3, 3, 2, 'fa-database', 'Categories', 'categories', NULL, '2017-11-30 17:17:19', '2017-12-14 09:11:18'),
(4, 2, 3, 'fa-database', 'CustomeAdminController', 'Custome', NULL, '2017-12-01 16:45:05', '2017-12-14 09:11:18'),
(5, 4, 3, 'fa-database', 'ManageCategories', 'Categories', NULL, '2017-12-01 17:04:18', '2017-12-14 09:11:18'),
(6, 5, 3, 'fa-list-alt', 'ManageCampaigns', 'Campaigns', NULL, '2017-12-06 06:18:59', '2017-12-14 09:11:18'),
(7, 6, 3, 'fa-cubes', 'ManageNews', 'News', NULL, '2017-12-07 07:58:07', '2017-12-14 09:11:18'),
(8, 7, 3, 'fa-cutlery', 'ManageBlogs', 'Blogs', NULL, '2017-12-07 08:19:20', '2017-12-14 09:11:18'),
(9, 1, 3, 'fa-envelope', 'ManageContactUs', 'Messages', NULL, '2017-12-14 09:11:07', '2017-12-14 09:11:18');

-- --------------------------------------------------------

--
-- Table structure for table `menu_role`
--

CREATE TABLE IF NOT EXISTS `menu_role` (
  `menu_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `menu_role_menu_id_role_id_unique` (`menu_id`,`role_id`),
  KEY `menu_role_menu_id_index` (`menu_id`),
  KEY `menu_role_role_id_index` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_role`
--

INSERT INTO `menu_role` (`menu_id`, `role_id`) VALUES
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=48 ;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_10_28_105111_create_categories_table', 2),
(4, '2017_10_29_044347_create_campaigns_table', 3),
(5, '2017_10_29_055938_create_comments_table', 4),
(6, '2017_10_31_124641_update_table_campaign', 5),
(8, '2017_11_06_140533_add_current_money_to_campaigns', 6),
(9, '2017_11_07_141201_add_column_in_users', 7),
(10, '2017_11_09_234151_update_table_campaign_to_show', 8),
(11, '2017_11_11_104242_update_active_table_campaign', 9),
(12, '2017_11_12_125822_update_table_user_google', 10),
(13, '2017_11_19_141606_create_table_news', 11),
(14, '2017_11_24_000126_create_table_email', 12),
(16, '2017_11_29_152908_create_table_blog', 14),
(39, '2015_10_10_000000_create_menus_table', 15),
(40, '2015_10_10_000000_create_roles_table', 15),
(41, '2015_10_10_000000_update_users_table', 15),
(42, '2015_12_11_000000_create_users_logs_table', 15),
(43, '2016_03_14_000000_update_menus_table', 15),
(45, '2017_12_12_161948_add-total-campaigns', 16),
(46, '2017_12_14_141618_create_table_contact_us', 17),
(47, '2017_12_31_090848_add_link_to_campaigns', 18);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default-news.jpg',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `content`, `image`, `created_at`, `updated_at`) VALUES
(1, 'How You Can Join Greta Van Susteren in Saving Lives in Liberia', '<p>It would have been easy to stop there. But now Greta has a more ambitious goal: to provide Sampson’s home country of Liberia with a CT scanner to save even more lives.</p>\r\n\r\n<p>Surprising as it may be, the entire country of Liberia is without this crucial piece of medical equipment. Because of limitations like this, cases such as Sampson’s progress far beyond what they should because proper medical intervention can’t be made.</p>\r\n\r\n<p><strong>Greta wants to change this</strong>. <a href="https://www.gofundme.com/CT-Liberia" data-href="https://www.gofundme.com/CT-Liberia" class="markup--anchor markup--p-anchor" rel="noopener" target="_blank">And with her new GoFundMe</a>, already $88,000 has been raised out of an formidable goal of $275,000. But there’s still a way to go to ensure the success of this critical project.</p>\r\n\r\n<p>A donation to this campaign isn’t just a generous gift&#8202;—&#8202;it’s also a vital investment in the health and livelihood of thousands of people who, through the random circumstances of their birth, don’t have access to a technology many of us take for granted.</p>\r\n\r\n<p>The campaign to save Sampson’s life showed the power of a group of people to make a real difference. It’s time to do it again&#8202;—&#8202;and have a lasting impact on an entire country.</p>', '1.jpg', NULL, NULL),
(2, 'This is title', '<p>It would have been easy to stop there. But now Greta has a more ambitious goal: to provide Sampson’s home country of Liberia with a CT scanner to save even more lives.</p>\r\n\r\n<p>Surprising as it may be, the entire country of Liberia is without this crucial piece of medical equipment. Because of limitations like this, cases such as Sampson’s progress far beyond what they should because proper medical intervention can’t be made.</p>\r\n\r\n<p><strong>Greta wants to change this</strong>. <a href="https://www.gofundme.com/CT-Liberia" data-href="https://www.gofundme.com/CT-Liberia" class="markup--anchor markup--p-anchor" rel="noopener" target="_blank">And with her new GoFundMe</a>, already $88,000 has been raised out of an formidable goal of $275,000. But there’s still a way to go to ensure the success of this critical project.</p>\r\n\r\n<p>A donation to this campaign isn’t just a generous gift&#8202;—&#8202;it’s also a vital investment in the health and livelihood of thousands of people who, through the random circumstances of their birth, don’t have access to a technology many of us take for granted.</p>\r\n\r\n<p>The campaign to save Sampson’s life showed the power of a group of people to make a real difference. It’s time to do it again&#8202;—&#8202;and have a lasting impact on an entire country.</p>', '2.jpg', NULL, NULL),
(3, 'this is another title', '<p>It would have been easy to stop there. But now Greta has a more ambitious goal: to provide Sampson’s home country of Liberia with a CT scanner to save even more lives.</p>\r\n\r\n<p>Surprising as it may be, the entire country of Liberia is without this crucial piece of medical equipment. Because of limitations like this, cases such as Sampson’s progress far beyond what they should because proper medical intervention can’t be made.</p>\r\n\r\n<p><strong>Greta wants to change this</strong>. <a href="https://www.gofundme.com/CT-Liberia" data-href="https://www.gofundme.com/CT-Liberia" class="markup--anchor markup--p-anchor" rel="noopener" target="_blank">And with her new GoFundMe</a>, already $88,000 has been raised out of an formidable goal of $275,000. But there’s still a way to go to ensure the success of this critical project.</p>\r\n\r\n<p>A donation to this campaign isn’t just a generous gift&#8202;—&#8202;it’s also a vital investment in the health and livelihood of thousands of people who, through the random circumstances of their birth, don’t have access to a technology many of us take for granted.</p>\r\n\r\n<p>The campaign to save Sampson’s life showed the power of a group of people to make a real difference. It’s time to do it again&#8202;—&#8202;and have a lasting impact on an entire country.</p>', '3.jpg', NULL, NULL),
(4, 'this is another title again', '<p>It would have been easy to stop there. But now Greta has a more ambitious goal: to provide Sampson’s home country of Liberia with a CT scanner to save even more lives.</p>\r\n\r\n<p>Surprising as it may be, the entire country of Liberia is without this crucial piece of medical equipment. Because of limitations like this, cases such as Sampson’s progress far beyond what they should because proper medical intervention can’t be made.</p>\r\n\r\n<p><strong>Greta wants to change this</strong>. <a href="https://www.gofundme.com/CT-Liberia" data-href="https://www.gofundme.com/CT-Liberia" class="markup--anchor markup--p-anchor" rel="noopener" target="_blank">And with her new GoFundMe</a>, already $88,000 has been raised out of an formidable goal of $275,000. But there’s still a way to go to ensure the success of this critical project.</p>\r\n\r\n<p>A donation to this campaign isn’t just a generous gift&#8202;—&#8202;it’s also a vital investment in the health and livelihood of thousands of people who, through the random circumstances of their birth, don’t have access to a technology many of us take for granted.</p>\r\n\r\n<p>The campaign to save Sampson’s life showed the power of a group of people to make a real difference. It’s time to do it again&#8202;—&#8202;and have a lasting impact on an entire country.</p>', '4.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('minh@yopmail.com', '$2y$10$Xlb07bZSx8uB1nFxRuSZnudFuPwsaDOGNvKpfN/t9QLy3X0shODt2', '2018-01-15 10:27:16');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '2017-11-30 16:54:09', '2017-11-30 16:54:09'),
(2, 'User', '2017-11-30 16:54:09', '2017-11-30 16:54:09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sex` enum('male','female') COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=30 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `sex`, `address`, `mobile`, `fullname`, `age`, `email`, `avatar`, `password`, `remember_token`, `created_at`, `updated_at`, `google_id`) VALUES
(1, NULL, 'minh', 'female', 'ZDFS SFSFSF D G', '23445678', 'F FF D F', 22, 'minh@yopmail.com', '1.jpg', '$2y$10$tQ9wFDEuxmboOb4mYfPe2eSlibKI43D9aIcxUNybtMba2Wh.OCDMC', 'psz0SRTJNPlnbscTmb4K3JQxGu28TQ9Y8L0PAX34NmyNO1U7iEcGMY73UelC', '2017-10-22 05:50:44', '2017-11-26 15:33:38', ''),
(8, NULL, 'min', NULL, NULL, NULL, NULL, NULL, 'minh1@yopmail.com', 'default.jpg', '$2y$10$GOT.HQn/XXuJ5lDjz6.79OJuB2bUykGPWf8817r3ec0TtVKjNc/yu', '1s0U2VCrtmwUc3nE1eOFSP8fJoi1HlWwwD23yxlOF3qNuIlxviGkqdQK9Q9e', '2017-11-16 16:13:25', '2017-11-16 16:13:25', NULL),
(9, NULL, 'minh2', NULL, NULL, NULL, NULL, NULL, 'minh2@yopmail.com', 'default.jpg', '$2y$10$giGC984hh2UOJ7ryfmHBI.emCLIwmGD29PtmAq5y4lu/4BaeJGk1a', 'AcmsbUnPv328WwHjw0lqI5X80uuc6zOS5ph6HZqFpV9IArTtcYdgcg1hX0Sa', '2017-11-16 16:18:55', '2017-11-16 16:18:55', NULL),
(10, NULL, 'minh3', NULL, NULL, NULL, NULL, NULL, 'minh3@yopmail.com', 'default.jpg', '$2y$10$gxyNN2YhjW7/VgSgLJmR.OcC6pnlpLSqVUt7h1rvPszSUVe0ZRmxm', 'gXCny4eCf9dzIfgorfgbHYKXgvnfQmkbIWo8a6xDZbrWv7Ev7vWcRgegJxXH', '2017-11-16 16:23:25', '2017-11-16 16:23:25', NULL),
(15, NULL, 'minh4', NULL, NULL, NULL, NULL, NULL, 'minh4@yopmail.com', 'default.jpg', '$2y$10$tUJhpFAlMclEO29bFGl1r.FtAOpahYUbhNqRdspeiU45Yl8Li.4Hm', 'KaQyW1re5KYGJkqXhY665mxQ51MpV02A9eU4wPAOwx3ogFCmLKNMTA5kYIRb', '2017-11-24 18:45:03', '2017-11-24 18:45:03', NULL),
(16, NULL, 'tam', NULL, NULL, NULL, NULL, NULL, 'tam1@yopmail.com', 'default.jpg', '$2y$10$SfauT3MVAPxdF5bWs6tOl.hb3u4RUGNgT5YeBh6fRYjfQXs/KPuZK', '7awx2MZYjUc5xEV5oDIiIcvc2rq2nEQWeDKpvqPk8Vt9J8PXQTns34J0RL3z', '2017-11-24 21:19:32', '2017-11-24 21:19:32', NULL),
(17, NULL, 'tran tam', NULL, NULL, NULL, 'tran tam', NULL, 'viuocmo.com@gmail.com', '17.jpg', '$2y$10$kHOSSRwAfcH05BVCxfQtyuSspAiCFG9RGFxq/xymLsyA5Cxn7Tp5u', '3z3CK7Tui5YmSuCOiJovviQA7gLrLeiMnbY8veIqJO2XYlmKVKKo0tnTKF58', '2017-11-24 21:21:26', '2017-11-24 21:21:27', '107486791938123442213'),
(18, 1, 'administrator', 'male', 'cần thơ việt nam', '+84936666666', 'Trần Thanh Tâm', 28, 'minhnguyen.admin@yopmail.com', '18.jpg', '$2y$10$tQ9wFDEuxmboOb4mYfPe2eSlibKI43D9aIcxUNybtMba2Wh.OCDMC', '9Jco0urkuhco51Qoh85cmVLdlEQPp45XQ3vYQFDSmngq4xUXIwvbG35jMR8K', '2017-11-30 16:55:39', '2018-03-07 06:48:36', NULL),
(19, NULL, 'JusikaDab', NULL, NULL, NULL, NULL, NULL, 'jusikaverg@poreglot.ru', 'default.jpg', '$2y$10$dhs5dKGDyTcwAIai4Xc7duE7EXSmEL8Ld/j7sdhknIO5t6QhtmISi', NULL, '2018-01-20 22:10:35', '2018-01-20 22:10:35', NULL),
(20, NULL, 'agrohimpba', NULL, NULL, NULL, NULL, NULL, 'lid.erpr.omo.2015super@gmail.com', 'default.jpg', '$2y$10$QJMx6rHrlzLSZTwUMhsADumZOf23QwZijb0pLrWS56nT5VbnqVc1C', NULL, '2018-01-29 11:57:01', '2018-01-29 11:57:01', NULL),
(21, NULL, 'Servicesct', NULL, NULL, NULL, NULL, NULL, 'bo.ri.s.19.77.g.o.rb.un.ov@gmail.com', 'default.jpg', '$2y$10$cinD4WkmQCegH4Y3glGUA.g0ec1lscrNrL2yJbPVLJt1aDpAuL7Me', NULL, '2018-02-07 11:40:25', '2018-02-07 11:40:25', NULL),
(22, NULL, 'Patriotksu', NULL, NULL, NULL, NULL, NULL, 'pl.e.n.ki.s.f.il.mby@gmail.com', 'default.jpg', '$2y$10$ckMoGFDdYNvjqeAdfrqzr.3cWigOVxAcJg1d38qOLC6HgaAXwCbn.', NULL, '2018-02-10 08:40:00', '2018-02-10 08:40:00', NULL),
(23, NULL, 'Bogdansew', NULL, NULL, NULL, NULL, NULL, 'b.l.iz.koy.a.n.2017@gmail.com', 'default.jpg', '$2y$10$gGX2/oNIw1vJJR8e/PVTwu8tOe1i7AZlq59t9DZfr7q6NgiF20Moy', NULL, '2018-02-13 10:52:19', '2018-02-13 10:52:19', NULL),
(24, NULL, 'tam tran', NULL, NULL, NULL, NULL, NULL, 'tamthanhtran_89@yahoo.com', 'default.jpg', '$2y$10$18osqxscPq5tPGWfJASCmeSYRmzf.wq9kPeiO/qiaBedr/2CusXfK', 'sAj352lvxB3FtiNNboQmiFqSN3ojaQpn6ZX5zQzhHw119ZUuG7fJc9Rrw2Bh', '2018-03-02 19:43:34', '2018-03-02 19:43:34', NULL),
(25, NULL, 'motmi', NULL, NULL, NULL, NULL, NULL, 'motmi1991@yopmail.com', 'default.jpg', '$2y$10$PaAgmXWx7rOKEQXYCWbtOur7WTl1MxM9Qp9PXbLpxttBMJ0fosn9u', 'ls4embg5Y9qPb1zfLDoHxowynB5iyxyDhuai1wvRAmpaLv4pWaIIRy7UdjTj', '2018-03-03 14:37:31', '2018-03-03 14:37:31', NULL),
(26, NULL, 'minhnguyen77', NULL, NULL, NULL, NULL, NULL, 'minhnguyen77@yopmail.com', 'default.jpg', '$2y$10$Ut8pKIOFyx8jFU7cnpFINetWeJiH2xTwfN8akPptddAtnYIZSojce', NULL, '2018-03-03 21:00:03', '2018-03-03 21:00:03', NULL),
(27, NULL, 'Karlosoaf', NULL, NULL, NULL, NULL, NULL, 's.hin.a.mi.n.s.k2.0.15@gmail.com', 'default.jpg', '$2y$10$eVrVeXTUc5XILNDp.ocOleRF37PpqjUqlqOtvugAvbrcnZMzEho6O', NULL, '2018-03-04 22:39:11', '2018-03-04 22:39:11', NULL),
(28, NULL, 'cowboy', NULL, NULL, NULL, NULL, NULL, 'cowboyspace@yopmail.com', 'default.jpg', '$2y$10$1mIb4Cck3mMJBXTbH.3J/u0fvjuamEpvZ7Ve1lIg1ndAxU6DlvciW', NULL, '2018-03-06 11:32:30', '2018-03-06 11:32:30', NULL),
(29, NULL, 'Andreasklr', NULL, NULL, NULL, NULL, NULL, 'boris19.8.0sec.en.o.v@gmail.com', 'default.jpg', '$2y$10$GxpGwxCfVd3d1ynDnxp7QuoBItBJI5SkulbiIZKGjbzxhzM0GKT8W', NULL, '2018-04-04 05:18:01', '2018-04-04 05:18:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_logs`
--

CREATE TABLE IF NOT EXISTS `users_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `action` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_model` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=24 ;

--
-- Dumping data for table `users_logs`
--

INSERT INTO `users_logs` (`id`, `user_id`, `action`, `action_model`, `action_id`, `created_at`, `updated_at`) VALUES
(1, 18, 'updated', 'users', 18, '2017-11-30 16:58:45', '2017-11-30 16:58:45'),
(2, 18, 'updated', 'users', 18, '2017-11-30 17:14:02', '2017-11-30 17:14:02'),
(3, 18, 'updated', 'users', 18, '2017-12-06 06:06:15', '2017-12-06 06:06:15'),
(4, 18, 'updated', 'users', 18, '2017-12-16 07:06:40', '2017-12-16 07:06:40'),
(5, 18, 'updated', 'users', 18, '2017-12-16 07:06:54', '2017-12-16 07:06:54'),
(6, 1, 'updated', 'users', 1, '2017-12-16 07:11:31', '2017-12-16 07:11:31'),
(7, 18, 'updated', 'users', 18, '2017-12-16 07:13:15', '2017-12-16 07:13:15'),
(8, 18, 'updated', 'users', 18, '2017-12-16 07:13:31', '2017-12-16 07:13:31'),
(9, 18, 'updated', 'users', 18, '2017-12-16 07:19:40', '2017-12-16 07:19:40'),
(10, 18, 'updated', 'users', 18, '2017-12-16 07:20:32', '2017-12-16 07:20:32'),
(11, 1, 'updated', 'users', 1, '2017-12-16 07:21:31', '2017-12-16 07:21:31'),
(12, 1, 'updated', 'users', 1, '2018-01-15 07:53:36', '2018-01-15 07:53:36'),
(13, 1, 'updated', 'users', 1, '2018-01-15 10:18:21', '2018-01-15 10:18:21'),
(14, 1, 'updated', 'users', 1, '2018-01-15 10:36:31', '2018-01-15 10:36:31'),
(15, 1, 'updated', 'users', 1, '2018-01-15 11:28:22', '2018-01-15 11:28:22'),
(16, 1, 'updated', 'users', 1, '2018-01-15 16:38:15', '2018-01-15 16:38:15'),
(17, 1, 'updated', 'users', 1, '2018-01-17 22:02:03', '2018-01-17 22:02:03'),
(18, 25, 'updated', 'users', 25, '2018-03-03 14:38:12', '2018-03-03 14:38:12'),
(19, 25, 'updated', 'users', 25, '2018-03-03 16:18:44', '2018-03-03 16:18:44'),
(20, 24, 'updated', 'users', 24, '2018-03-03 16:29:40', '2018-03-03 16:29:40'),
(21, 17, 'updated', 'users', 17, '2018-03-05 16:05:19', '2018-03-05 16:05:19'),
(22, 24, 'updated', 'users', 24, '2018-03-06 11:37:21', '2018-03-06 11:37:21'),
(23, 18, 'updated', 'users', 18, '2018-03-07 06:48:36', '2018-03-07 06:48:36');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `menu_role`
--
ALTER TABLE `menu_role`
  ADD CONSTRAINT `menu_role_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `menu_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;