<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            $table->string('avatar')->default('default.jpg')->after('email');
            $table->integer('age')->nullable()->after('name');
            $table->string('fullname')->nullable()->after('name');
            $table->string('mobile')->nullable()->after('name');
            $table->string('address')->nullable()->after('name');
            $table->enum('sex', ['male', 'female'])->nullable()->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function($table) {
            $table->dropColumn('avatar');
            $table->dropColumn('age');
            $table->dropColumn('fullname');
            $table->dropColumn('mobile');
            $table->dropColumn('address');
            $table->dropColumn('sex');
        });
    }
}
