<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableCampaignToShow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('campaigns', function (Blueprint $table) {
            $table->enum('type_upload', ['url_youtube', 'photo'])->default('photo')->after('category_id');
            $table->string('upload')->default('default_campaign.jpg')->after('category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('campaigns', function($table) {
            $table->dropColumn('type_upload');
            $table->dropColumn('upload');
        });
    }
}
