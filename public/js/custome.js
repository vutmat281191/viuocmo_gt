//facebook share
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.11';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

//google map
function myMap() {
    var myCenter = new google.maps.LatLng(51.508742, -0.120850);
    var mapCanvas = document.getElementById("map");
    var mapOptions = { center: myCenter, zoom: 12 };
    var map = new google.maps.Map(mapCanvas, mapOptions);
    var marker = new google.maps.Marker({ position: myCenter });
    marker.setMap(map);
}

function changeMenuFooter(id) {
    var x = document.getElementById(id);
    if (x.className === "menuFooter") {
        x.className += " responsive";
    } else {
        x.className = "menuFooter";
    }
}

function changeStatusCampaign(id) {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        url: './change-status-campaign',
        data: {
            campaign_id: id,
        },
        success: function(data) {
            // $("#msg").html(data.msg);
            console.log(data.msg);

        }
    });
}
$(document).ready(function() {
    tinymce.init({
        selector: '#tinyMce',
        height: 500,
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code help'
        ],
        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
    });

    tinymce.init({
        selector: '#sub_tinyMce',
        height: 500,
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code help'
        ],
        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
    });

    $('.part_type_upload .select_type_upload').change(function() {
        if ($(this).val() == 'photo') {
            $('.part_upload_youtube').hide();
            $('.part_upload_photo').show();
        } else {
            $('.part_upload_photo').hide();
            $('.part_upload_youtube').show();
        }
    });

    $(".currency-control-bc").change(function() {
        var profits = this.value.replace(/\./g, '');
        $(this).val(profits.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
        $('.currency-control').val($(this).val().replace(/\./g, ""));
    });

});