@extends('admin.layouts.master')

@section('content')
<div class="jumbotron banner-static-pages">
	<h1 class="title-static-page">Create category</h1>
	<p class="desc-static-page">GoFundMe is the World's #1 Personal Fundraising Website.</p>
</div>
<div class="container">
	<div class="row">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
	</div>
	<div class="row">
		{{ Form::open(['url' => 'submit-create-campaign-admin', 'files' => true]) }}
		{!! Form::token() !!}
			<input type="hidden" name="id_campaign" value="{{ $campaign->id }}">
			<div class="col-md-12 part_type_upload">
				{{ Form::label('type_upload', 'Tupe upload:') }}
				{!! Form::select('type_upload', ['url_youtube' => 'URL Youtube', 'photo' => 'Photo'], $campaign->type_upload, ['class' => 'form-control select_type_upload', 'placeholder' => 'Slect type upload']) !!}
			</div>
			<div class="col-md-12 part_upload_photo">
				{{ Form::label('upload_photo', 'Update photo:') }}
				{!! Form::file('upload_photo', ['class' => 'form-control']) !!}
			</div>
			<div class="col-md-12 part_upload_youtube">
				{{ Form::label('upload_url', 'URL Youtube:') }}
				{!! Form::text('upload_url', $campaign->upload_url, ['class' => 'form-control']) !!}
			</div>
			<div class="col-md-12">
				{{ Form::label('money_target', 'Your target:') }}
				{!! Form::number('money_target', $campaign->money_target, ['class' => 'form-control currency-control', 'step'=>'100000']) !!}
			</div>
			<div class="col-md-12">
				{{ Form::label('title', 'Title:') }}
				{!! Form::text('title', $campaign->title, ['class' => 'form-control']) !!}
			</div>
			<div class="col-md-12">
				<?php
					foreach($categories as $cat){
					    $cates[$cat->id] = $cat->title;
					}
				?>
				{{ Form::label('category', 'category:') }}
				{!! Form::select('category', $cates, $campaign->category_id, ['placeholder' => 'Pick a category', 'class' => 'form-control']) !!}
			</div>
			<div class="col-md-12">
				{{ Form::label('content', 'Content:') }}
				{!! Form::textarea('content', $campaign->content, ['id'=>'tinyMce', 'class'=>'form-control']) !!}
			</div>
			<div class="col-md-12">
				{{ Form::submit('SUBMIT', ['class'=>'button-default form-control']) }}
			</div>
		{!! Form::close() !!}
	</div>
</div>
@endsection
