@extends('admin.layouts.master')

@section('content')

	<!-- button add categories -->
    <p><a href="{{ url('admin/category/create') }}" class="btn btn-success">{!! trans('quickadmin::admin.add-category') !!}</a></p>

    @if($categories->count() > 0)
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ trans('quickadmin::admin.users-index-users_list') }}</div>
            </div>
            <div class="portlet-body">
                <table id="datatable" class="table table-striped table-hover table-responsive datatable">
                    <thead>
                    <tr>
                        <th>{{ trans('quickadmin::admin.categories-index-name') }}</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($categories as $category)
                        <tr>
                            <td>{{ $category->title }}</td>
                            <td>
                                <a href="{{ url('admin/category/edit') }}/{{ $category->id }}" class="btn btn-xs btn-info">{!! trans('quickadmin::admin.users-index-edit') !!}</a>
                                <a href="{{ url('admin/category/delete') }}/{{ $category->id }}" class="btn btn-xs btn-danger" style="display: none;">{!! trans('quickadmin::admin.users-index-delete') !!}</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @else
        {{ trans('quickadmin::admin.users-index-no_entries_found') }}
    @endif

@endsection