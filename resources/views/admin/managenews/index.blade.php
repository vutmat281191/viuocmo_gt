@extends('admin.layouts.master')

@section('content')

	<!-- button add categories -->
    <p><a href="{{ url('admin/news/create') }}" class="btn btn-success">{!! trans('quickadmin::admin.add-new') !!}</a></p>

    @if($news->count() > 0)
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ trans('quickadmin::admin.new-index-new_list') }}</div>
            </div>
            <div class="portlet-body">
                <table id="datatable" class="table table-striped table-hover table-responsive datatable">
                    <thead>
                    <tr>
                        <th>{{ trans('quickadmin::admin.news-index-name') }}</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($news as $new)
                        <tr>
                            <td>{{ $new->title }}</td>
                            <td>
                                <a href="{{ url('admin/news/edit') }}/{{ $new->id }}" class="btn btn-xs btn-info">{!! trans('quickadmin::admin.users-index-edit') !!}</a>
                                <a href="{{ url('admin/news/delete') }}/{{ $new->id }}" class="btn btn-xs btn-danger">{!! trans('quickadmin::admin.users-index-delete') !!}</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @else
        {{ trans('quickadmin::admin.users-index-no_entries_found') }}
    @endif

@endsection