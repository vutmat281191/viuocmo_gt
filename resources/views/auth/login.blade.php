@include('admin.partials.header')
<div style="margin-top: 10%;"></div>
<div class="container-fluid ">
    <div class="row overview-login-form">
        <div class="col-md-8 col-md-offset-2 ">
            <div class="panel panel-default login-form">
                <div class="panel-heading panel-background">Đăng Nhập</div>
                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>{{ trans('quickadmin::auth.whoops') }}</strong> {{ trans('quickadmin::auth.some_problems_with_input') }}
                            <br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="form-horizontal"
                          role="form"
                          method="POST"
                          action="{{ url('login') }}">
                        <input type="hidden"
                               name="_token"
                               value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label class="col-md-4 control-label">Email</label>

                            <div class="col-md-6 form-email">
                                <input type="email"
                                       class="form-control-email"
                                       name="email"
                                       value="{{ old('email') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Mật Khẩu</label>

                            <div class="col-md-6 form-password">
                                <input type="password"
                                       class="form-control-password"
                                       name="password">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <label>
                                    <input type="checkbox"
                                           name="remember">Ghi Nhớ
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="{{ url('password/reset') }}">Quên Mật Khẩu</a>
                            </div>
                        </div>
                        

                        <div class="form-group">
                            <div class="submit-btn">
                                <button type="submit"
                                        class="btn btn-primary login-btn"
                                        style="margin-right: 15px;">
                                        Đăng Nhập
                                </button>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            
                            <p class="extension-choosen">Hoặc có thể đăng nhập bằng :</p>
                        </div>
                        <div class="form-group">
                            <div class=" btn-google ">
                                <a class="google-button-btn" href="{{ url('auth/google') }}">GOOGLE +</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.partials.footer')
