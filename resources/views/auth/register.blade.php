@include('admin.partials.header')
<div style="margin-top: 10%;"></div>
<div class="container-fluid">
    <div class="row overview-login-form">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default login-form">
                <div class="panel-heading">Đăng Kí Thành Viên</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">NickName</label>

                            <div class="col-md-6 regedit-form">
                                <input id="name" type="text" class="form-control-name" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Địa Chỉ Email</label>

                            <div class="col-md-6 regedit-form">
                                <input id="email" type="email" class="form-control-email-regedit" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Mật Khẩu</label>

                            <div class="col-md-6 regedit-form">
                                <input id="password" type="password" class="form-control-password-regedit" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="password-confirm" class="col-md-4 control-label">Xác Nhận Mật Khẩu</label>
 
                            <div class="col-md-6 regedit-form">
                                <input id="password-confirm" type="password" class="form-control-re-password-regedit" name="password_confirmation" required>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary login-btn-regedit">
                                Đăng Kí
                                </button>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            
                            <p class="extension-choosen">Hoặc có thể đăng nhập bằng :</p>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4 btn-google-regedit ">
                                <a class="google-button-btn-regedit" href="{{ url('auth/google') }}">GOOGLE +</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.partials.footer')
