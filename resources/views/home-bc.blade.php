@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-5 col-xs-12 title-home-wrapper">
			<p class="title-home">Raise money for yourself, others, and charities</p>
			<a class="button-m button-default" href="{{ url('create-campaign') }}">Start a fun</a>
			<button class="button-m button-custome">How it works</button>
		</div>
		<div class="col-md-7 col-xs-12">
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner">
					<div class="item active">
						<img src="http://epilepsyu.com/wp-content/uploads/2014/01/happy-people-1050x600.jpg" alt="Los Angeles" style="width:100%;">
					</div>

					<div class="item">
						<img src="http://dreamatico.com/data_images/people/people-2.jpg" alt="Chicago" style="width:100%;">
					</div>
				
					<div class="item">
						<img src="http://itsaboutpeople.co.za/wp-content/uploads/2017/02/business-people.jpg" alt="New york" style="width:100%;">
					</div>
				</div>

				<!-- Left and right controls -->
				<a class="left carousel-control" href="#myCarousel" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#myCarousel" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
	</div>
</div>
@endsection
