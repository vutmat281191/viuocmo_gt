<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'GT') }}</title>

	<!-- Styles -->
	<!-- <link href="{{ url('public/css/app.css') }}" rel="stylesheet"> -->
	<link href="{{ url('public/css/app.css') }}" rel="stylesheet">
	<link href="{{ url('public/css/custome.css') }}" rel="stylesheet">
	<link href="{{ url('public/css/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
</head>
<body>
	<div class="overview">

		<nav class="navbar navbar-default navbar-static-top">
			<div class="container">

			<div class="col-lg-2 col-md-2 col-sm-3 navbar-header">

					<!-- Collapsed Hamburger -->
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
						<span class="sr-only">Toggle Navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<!-- Branding Image -->
					<a class="navbar-brand" href="{{ url('/') }}">
						<img src="{{ url('public/uploads/logo_sq.png') }}" title="{{ config('app.name', 'GT project') }}" alt="{{ config('app.name', 'GT project') }}">
					</a>
				</div>
				<div class="collapse navbar-collapse row" id="app-navbar-collapse">
					<!-- LoGo -->

					<!-- Left Side Of Navbar -->
					<div class="col-lg-4 col-md-10 col-sm-9">
					{{ Form::open(['url' => 'submit-search-campaign','class' =>'form-search-template-wrapper']) }}
				      {!! Form::token() !!}

				        {!! Form::text('search', null, ['placeholder' => 'Tìm kiếm chiến dịch, tiêu đề ...', 'class' => 'form-control search-campaign-bar']) !!}
						<button type="submit">Tìm Kiếm</button>

				   {!! Form::close() !!}
						<!-- {{ Form::open(['url' => 'submit-search-campaign', 'class' => 'search-form-wrapper cf']) }}
						{!! Form::token() !!}
							<div class="col-md-12 ">
							{!! Form::text('search', null, ['placeholder' => 'Searching Campain Name', 'class' => 'form-control search-campaign-bar']) !!}
								{{ Form::submit('SUBMIT', ['class' => 'button searching']) }}
							</div>
						{!! Form::close() !!} -->
					</div>

					<!-- Right Side Of Navbar -->
					<ul class="col-lg-6 col-md-12 col-sm-12 nav navbar-nav navbar-right">
						<li><a href="{{ url('create-campaign') }}">Tạo chiến dịch ngay</a></li>
						<!-- Authentication Links -->
						@guest
							<li><a href="{{ url('how-it-works') }}">Cách làm việc</a></li>
							<li><a href="{{ url('contact-us') }}">Giúp đỡ</a></li>
							<li><a href="{{ url('login') }}">Đăng nhập</a></li>
							<li><a href="{{ url('register') }}">Đăng ký</a></li>
						@else
							<li><a href="{{ url('how-it-works') }}">Cách hoạt động</a></li>
							<li><a href="{{ url('contact-us') }}">Giúp đỡ</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
									<img src="{{ url('public/uploads/avatars') }}/{{ Auth::user()->avatar }}" style="width:20px; height:20px; border-radius:50%; margin-right:7px;">
									{{ Auth::user()->name }} <span class="caret"></span>
								</a>

								<ul class="dropdown-menu" role="menu">
									<li><a href="{{ url('my-account') }}">Hồ sơ của tôi</a></li>
									<li><a href="{{ url('login') }}">Ví của tôi</a></li>
									<li><a href="{{ url('my-campaigns') }}">Chiến dịch của tôi</a></li>
									<li><a href="{{ url('change-password') }}">Thay đổi mật khẩu</a></li>
									<li>
										<a href="{{ route('logout') }}"
											onclick="event.preventDefault();
													 document.getElementById('logout-form').submit();">
											Đăng xuất
										</a>

										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
											{{ csrf_field() }}
										</form>
									</li>
								</ul>
							</li>
						@endguest
					</ul>
				</div>
			</div>
			<div class="jumbotron list-categories-wrapper">
				<div class="container">
					<ul class="list-categories ">

						@foreach ($categories as $cat)
						    <li class="Category-item">

						    	<a href="{{ url('categories') }}/{{ $cat->id }}">{{ $cat->title }}</a>
						    </li>
						@endforeach
					</ul>
				</div>
			</div>
		</nav>
		<div id="fb-root"></div>
		@yield('content')
		<div class="footer">
			<div class="footer-wrapper container">
				<div class="row menu-footer-custome">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 menuFooter">
						<div class="get-started" id="footer_get_started">
							<a class="Leader " href="#">Hướng dẫn</a>
							<li><a href="{{ url('how-it-works') }}"><i class="fa fa-angle-right"></i>Cách hoạt động</a></li>
							<li><a href="{{ url('fee') }}"><i class="fa fa-angle-right"></i>Giá và phí</a></li>
							<li><a href="{{ url('register') }}"><i class="fa fa-angle-right"></i>Đăng kí</a></li>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 menuFooter">
						<div class="tio-categories" id="footer_top_categories">
							<a class="Leader " href="#">Top chủ đề</a>
							<li><a href="{{ url('categories/1') }}"><i class="fa fa-angle-right"></i>Điều trị bệnh</a></li>
							<li><a href="{{ url('categories/6') }}"><i class="fa fa-angle-right"></i>Ước mơ</a></li>
							<li><a href="{{ url('categories/2') }}"><i class="fa fa-angle-right"></i>Khẩn cấp</a></li>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 menuFooter">
						<div class="footer-introduction" id="footer_learn_more">
							<a class="Leader " href="{{ url('about-us') }}">Giới thiệu</a>
							<li><a href="{{ url('list-blogs') }}"><i class="fa fa-angle-right"></i>Blogs</a></li>
							<li><a href="{{ url('list-news') }}"><i class="fa fa-angle-right"></i>Tin tức</a></li>
							

						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 menuFooter">
						<div class="get-support" id="footer_get_support">
							<a class="Leader " href="{{ url('contact-us') }}">Giúp đỡ</a>							
							<li><a href="{{ url('we-are-hiring') }}"><i class="fa fa-angle-right"></i>Tuyển dụng</a></li>
							<li><a href="{{ url('contact-us') }}"><i class="fa fa-angle-right"></i>Liên hệ</a></li>
						</div>
					</div>
				</div>
				<div class="row footer_hr">
					<div class="col-md-6 col-xs-6 left-copyright-content">
						<span>© 2010-2017</span>
						<a class="left-copyright" href="{{ url('terms') }}">Điều khoản</a>
						<a class="left-copyright" href="{{ url('privacy') }}">Chính sách</a>
						<a class="left-copyright" href="{{ url('legal') }}">Luật</a>
					</div>
					<div class="col-md-6 col-xs-6 right-social">
						<a href="#"><i class="fa fa-facebook social-icon"></i></a>
						<a href="#"><i class="fa fa-youtube-play social-icon"></i></a>
						<a href="#"><i class="fa fa-instagram social-icon"></i></a>
						<a href="#"><i class="fa fa-skype social-icon"></i></a>
						<a href="#"><i class="fa fa-twitter social-icon"></i></a>

					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Scripts -->
	<!-- <script src="{{ url('public/js/app.js') }}"></script> -->
	<script src="{{url('public/js/app.js') }}"></script>
	<script src="{{url('public/js/custome.js') }}"></script>
	<script src="{{url('public/js/vendor/tinymce/jquery.tinymce.min.js') }}"></script>
	<script src="{{url('public/js/vendor/tinymce/tinymce.min.js') }}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8tMllfxdLcT49nbl0fk_uYozRdBEmuKs&callback=myMap"></script>
</body>
</html>
