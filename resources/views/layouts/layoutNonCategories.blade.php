<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'GT') }}</title>

	<!-- Styles -->
	<!-- <link href="{{ url('public/css/app.css') }}" rel="stylesheet"> -->
	<link href="{{ url('public/css/app.css') }}" rel="stylesheet">
	<link href="{{ url('public/css/custome.css') }}" rel="stylesheet">
	<link href="{{ url('public/css/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
</head>
<body>
	<div id="app">
		<nav class="navbar navbar-default navbar-static-top">
			<div class="container">
				<div class="navbar-header">

					<!-- Collapsed Hamburger -->
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
						<span class="sr-only">Toggle Navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<!-- Branding Image -->
					<a class="navbar-brand" href="{{ url('/') }}">
						{{ config('app.name', 'GT project') }}
					</a>
				</div>

				<div class="collapse navbar-collapse" id="app-navbar-collapse">
					<!-- Left Side Of Navbar -->
					<ul class="nav navbar-nav">
						&nbsp;
					</ul>

					<!-- Right Side Of Navbar -->
					<ul class="nav navbar-nav navbar-right">
						<li><a href="{{ url('create-campaign') }}">Start a Fundraiser</a></li>
						<!-- Authentication Links -->
						@guest
							<li><a href="{{ url('tour') }}">How it works</a></li>
							<li><a href="{{ url('tour') }}">Help</a></li>
							<li><a href="{{ url('login') }}">Login</a></li>
							<li><a href="{{ url('register') }}">Register</a></li>
						@else
							<li><a href="{{ url('tour') }}">How it works</a></li>
							<li><a href="{{ url('tour') }}">Help</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
									<img src="{{ url('public/uploads/avatars') }}/{{ Auth::user()->avatar }}" style="width:20px; height:20px; border-radius:50%; margin-right:7px;">
									{{ Auth::user()->name }} <span class="caret"></span>
								</a>

								<ul class="dropdown-menu" role="menu">
									<li><a href="{{ url('my-account') }}">My profile</a></li>
									<li><a href="{{ url('login') }}">My donations</a></li>
									<li><a href="{{ url('my-campaigns') }}">My campaigns</a></li>
									<li><a href="{{ url('change-password') }}">Change Password</a></li>
									<li>
										<a href="{{ route('logout') }}"
											onclick="event.preventDefault();
													 document.getElementById('logout-form').submit();">
											Logout
										</a>

										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
											{{ csrf_field() }}
										</form>
									</li>
								</ul>
							</li>
						@endguest
					</ul>
				</div>
			</div>
		</nav>
		<div id="fb-root"></div>
		@yield('content')
		<div class="footer container">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<div class="menuFooter" id="footer_get_started">
						<a href="#">Get Started</a>
						<a href="{{ url('how-it-works') }}">How It Works</a>
						<a href="{{ url('fee') }}">Pricing and Fees</a>
						<a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="changeMenuFooter('footer_get_started')">&#9776;</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<div class="menuFooter" id="footer_top_categories">
						<a href="#">Top Categories</a>
						<a href="#">Medical Fundraising</a>
						<a href="#">Memorial Fundraising</a>
						<a href="#">Charity Fundraising</a>
						<a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="changeMenuFooter('footer_top_categories')">&#9776;</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<div class="menuFooter" id="footer_learn_more">
						<a href="{{ url('about-us') }}">About Us</a>
						<a href="{{ url('list-blogs') }}">Blog</a>
						<a href="{{ url('contact-us') }}">Contact</a>
						<a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="changeMenuFooter('footer_learn_more')">&#9776;</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<div class="menuFooter" id="footer_get_support">
						<a href="{{ url('contact-us') }}">Help Center</a>
						<a href="{{ url('list-news') }}">News</a>
						<a href="{{ url('we-are-hiring') }}">We're hiring</a>
						<a href="{{ url('contact-us') }}">Contact Us</a>
						<a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="changeMenuFooter('footer_get_support')">&#9776;</a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-xs-6 left-copyright">
					<span>© 2010-2017</span>
					<a href="{{ url('terms') }}">Terms</a>
					<a href="{{ url('privacy') }}">Privacy</a>
					<a href="{{ url('legal') }}">Legal</a>
				</div>
				<div class="col-md-6 col-xs-6 right-social">
					<a href="#"><i class="fa fa-facebook-square social-icon"></i></a>
					<a href="#"><i class="fa fa-youtube-play social-icon"></i></a>
				</div>
			</div>
		</div>
	</div>

	<!-- Scripts -->
	<!-- <script src="{{ url('public/js/app.js') }}"></script> -->
	<script src="{{ url('public/js/app.js') }}"></script>
	<script src="{{ url('public/js/custome.js') }}"></script>
	<script src="{{ url('public/js/vendor/tinymce/jquery.tinymce.min.js') }}"></script>
	<script src="{{ url('public/js/vendor/tinymce/tinymce.min.js') }}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8tMllfxdLcT49nbl0fk_uYozRdBEmuKs&callback=myMap"></script>
</body>
</html>
