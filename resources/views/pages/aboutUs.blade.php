@extends('layouts.app')

@section('content')
<div class="container about-us">

<div class=" banner-static-pages">
	<h1 class="title-static-page">Đôi điều về tổ chức chúng tôi</h1>
	<p class="desc-static-page">ViUocMo là mô hình kết nối cộng đồng của người Việt Nam.</p>
</div>
	<div class="row about-us-img">
		<img src="https://i0.wp.com/cfci.org.in/wp-content/uploads/2015/12/BigFamily-min.jpg">
	</div>
	<div class="row content-about-us">
	<i class="fa fa-hand-o-right" aria-hidden="true"></i>
		<p>Trong xã hội hiện đại và phát triển như hiện nay, với nhịp sống bộn bề, tấp nập đang diễn ra hàng ngày, đâu đó vẫn còn không ít những mảnh đời kém may mắn, những hoàn cảnh khó khăn rất cần đến sự tương trợ, giúp đỡ của cộng đồng.
		Đồng cảm với những số phận ấy, cùng với mong muốn được sẻ chia, Dự án GT (*) của chúng tôi ra đời như một tổ chức từ thiện phi lợi nhuận và được xây dựng dựa trên tinh thần tự nguyện tương thân tương ái, sự đồng cảm, với mục tiêu chung tay hỗ trợ để giúp đỡ những mảnh đời bất hạnh có thể vượt qua được những khó khăn trong cuộc sống.</p>
		<p>Dự án GT của chúng tôi được thiết lập với mục đích là cầu nối giữa những mảnh đời, những hoàn cảnh khó khăn đang cần sự sẻ chia với những nhà hảo tâm, những trái tim thiện nguyện, đồng cảm...</p>
		<p>Tại Dự án GT, các thành viên có thể chia sẻ hoàn cảnh, những khó khăn mà chính bản thân đang gặp phải. Đây cũng là nơi các thành viên có thể chia sẻ, cung cấp lên thông tin về những hoàn cảnh khó khăn khác mà mình biết đang cần đến sự giúp đỡ, hỗ trợ của cộng đồng. Thông qua đó, các nhà hảo tâm đồng cảm với hoàn cảnh nào sẽ liên hệ tìm hiểu để biết rõ thêm và góp sức giúp đỡ cho những hoàn cảnh ấy.</p>
		<p>Với Dự án GT, chúng tôi kỳ vọng sẽ kết nối được các nhà hảo tâm có cùng tâm huyết, sẽ kêu gọi được nhiều những tấm lòng, những sự chung tay góp sức. Cũng như, những hoàn cảnh khó khăn, những mảnh đời kém may mắn sẽ được biết đến nhiều hơn và được kịp thời giúp đỡ để sớm vượt qua và có cuộc sống tốt đẹp hơn.</p>
		<p>Cùng với tinh thần đó, chúng tôi mong rằng Dự án GT sẽ nhận được sự đồng hành từ những nhà hảo tâm có cùng tâm huyết, cũng như thật nhiều sự chia sẻ về những hoàn cảnh khó khăn cần tượng trợ để Dự án GT có thể phát triển, đến gần với cộng đồng hơn, giúp đỡ được nhiều hoàn cảnh khó khăn trên toàn đất nước Việt Nam và có thể đạt được đúng mục tiêu, ý nghĩa giúp ích cho cộng đồng, cho xã hội.</p>
	</div>
</div>
@endsection
