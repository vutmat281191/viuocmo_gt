@extends('layouts.app')

@section('content')
<div class="jumbotron banner-static-pages news-blog ">
<div class="title-desc" >
	<h1 class="title-static-page">blog</h1>
	<p class="desc-static-page">ViUocMo là mô hình kết nối cộng đồng của người Việt Nam.</p>
</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="picture-news-wrapper">
				<img class="img-news-details" src="{{ url('public/uploads/blogs') }}/{!! $blog->image !!}">
			</div>
			<div class="title-news-details">
				<h3>{{ $blog->title }}</h3>
			</div>
			<div class="story-news">
				<h3>  Blogs - Story</h3>
				{!! $blog->content !!}
			</div>
			<div class="share-news"></div>
		</div>
		
		<div class="col-md-12">
			<div class="fb-share-button" 
			    data-href="{{ url('/blogs') }}/{{ $blog->id }}" 
			    data-layout="button_count">
			</div>
		</div>
	</div>
</div>
@endsection
