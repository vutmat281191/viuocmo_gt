@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
			<div class="col-md-8 campaign-left">
				<div class="picture-campaign-wrapper">
					{!! $url_media !!}
				</div>
				<div class="title-campaign">
					<h3>{{ $campaign->title }}</h3>
					<div class="dotted-line mr ml"></div>
				</div>

				<div class="share-campaign">
					<button type="button" class="btn btn-primary btn-lg facebook"><i class="fa fa-facebook" aria-hidden="true"></i>Share</button>
					<button type="button" class="btn btn-primary btn-lg twitter"><i class="fa fa-twitter" aria-hidden="true"></i>Tweet</button>
				</div>
				<!-- <div class="fb-share-button btn-sharing" 
				    data-href="{{ url('/campaign') }}/{{ $campaign->link }}" 
					data-layout="button_count" data-size="large">
					
				</div> -->
				<div class="dotted-line"></div>
				<div class="story-campaign">
					<h3>Câu Chuyện</h3>
					<!-- <div class="dotted-line mr ml"></div> -->
					{!! $campaign->content !!}
					<!-- <p class="read-more"><a href="#" class="button">Read More</a></p> -->
				</div>
				
				
			</div>
			<div class="col-md-4 campaign-right">
				<div class="money-campaign">$ {{ $campaign->money_target }}</div>
					<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
  						aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
    				</div>
 				 </div>
			<div class="producer-campaign">Raised by 101 people in 12 months</div>
				<div class="comments-campaign"></div>
				<div class="share-campaign-right">
					<button type="button" class="btn btn-primary btn-lg facebook"><i class="fa fa-facebook" aria-hidden="true"></i>Share</button>
					<button type="button" class="btn btn-primary btn-lg twitter"><i class="fa fa-twitter" aria-hidden="true"></i>Tweet</button>
				</div>
				<!-- <div class="fb-share-button" 
				    data-href="{{ url('/campaign') }}/{{ $campaign->link }}" 
				    data-layout="button_count" data-size="large">
				</div> -->
			</div>
	</div>
</div>
@endsection
