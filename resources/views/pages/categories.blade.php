@extends('layouts.app')

@section('content')
@foreach ($categories as $cat)
	@if ($cat->id == $id)
		<?php
			$content = $cat->content;
			$sub_content = $cat->sub_content;
		?>
		@break
	@endif
@endforeach
<div class="container categories-content-wrapper">
	<div class="row top-categories">
		<div class="col-md-8 page-category-slider-top">
			<div id="myCarousel" class="carousel slide home-slider" data-ride="carousel">
			<!-- Indicators -->
			<!-- Wrapper for slides -->
				<div class="carousel-inner picture-wrapper">
					<div class="item active">
						<img src="../public/uploads/Slider/Lib/2_1.jpg" alt="Los Angeles" >
						<!-- <h1 class="title-home">Raise money for yourself, others, and charities</h1>
						<a class="button-m button-default btn-home" href="{{ url('create-campaign') }}">Start a fun</a>
						<button class="button-m button-custome">How it works</button> -->
					</div>

					<div class="item">
						<img src="../public/uploads/Slider/Lib/3_1.jpg" alt="Chicago" >
						
					</div>
				
					<div class="item">
						<img src="../public/uploads/Slider/Lib/3_2.jpg" alt="New york" >
						
					</div>
					<div class="item">
						<img src="../public/uploads/Slider/Lib/3_3.jpg" alt="New york">
						
					</div>
				
				</div>
			
				<!-- Left and right controls -->
				<a class="left carousel-control" href="#myCarousel" data-slide="prev">
					
					<i class="fa fa-chevron-left" aria-hidden="true"></i>
				</a>
				<a class="right carousel-control" href="#myCarousel" data-slide="next">
					<i class="fa fa-chevron-right" aria-hidden="true"></i>
				</a>
			</div>
		</div>
		<div class="col-md-4 page-category-banner-top">
			<!-- <h2>Chi phí mua thuốc và chữa bệnh</h2>
			<p>Tạo chiến dịch để gây quỹ chi trả cho các chi phí Y Dược. Gây quỹ cho các hoạt động liên quan đến lĩnh vực Y Khoa.</p>
			<p>Mô hình gây quỹ cho các chi phí Y Dược duy nhất tại Việt Nam, tạo chiến dịch ngay trong vòng vài giây.</p> -->
			{!! $sub_content !!}

			<a class="button-m button-default " href="{{ url('create-campaign') }}">TẠO CHIẾN DỊCH</a>
		</div>
	</div>
	<div class= "row bottom-categories">
		
		<div class="content-category">
			{!! $content !!}
		</div>
		<div class="content-list-campaign">
			<h1>Các chiến dịch đang kêu gọi gây quỹ</h1>
			@foreach ($campaigns as $campaign)
				<div class="campaign-item col-lg-4 col-md-4 col-sm-4 col-xs-6">
					<div class="img-campaign-item">
						<?php
							if ($campaign->type_upload == 'photo') {
								echo '<img class="img_campaign" src="' . url("public/uploads/campaigns/" . $campaign->upload) . '">';
							} else {
								echo '<div class="embed-responsive embed-responsive-16by9 col-xs-12 text-center">' . $campaign->upload . '</div>';
							}
						?>
					</div>
					<h3 class="target-campaign-item">$ {{ $campaign->money_target }}</h3>
					<div class="campaign-item-bottom">
						<a href="{{ url('/campaign') }}/{{ $campaign->link }}">
							<h4 class="title-campaign-item">{{ $campaign->title }}</h4>
						</a>
						<div class="content-campaign-item">
						
							{!! $campaign->content !!}
							<p class="read-more"><a href="#" class="button">Read More</a></p>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
</div>
@endsection
