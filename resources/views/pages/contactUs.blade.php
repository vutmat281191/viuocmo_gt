@extends('layouts.app')

@section('content')
<div class="container">
<div class="banner-static-pages">
	<h1 class="title-about-us title-static-pages">Giúp Đỡ</h1>
	<p>Xin hãy để lại lời nhắn cho chúng tôi.</p>
</div>
<div class="container page-contact-us">
	<div class="row">
	    <div class="column">
	        <div id="map" style="width:100%;height:500px"></div>
	    </div>
	    <div class="column right-contact">
		    {{ Form::open(['url' => 'submit-create-contact-us']) }}
			{!! Form::token() !!}
				<div class="col-md-12">
					{{ Form::label('full_name', 'Full Name:') }}
					{!! Form::text('full_name', null, ['class' => 'form-control']) !!}
				</div>
				<div class="col-md-12">
					{{ Form::label('email', 'Email:') }}
					{!! Form::email('email', null, ['class' => 'form-control']) !!}
				</div>
				<div class="col-md-12">
					{{ Form::label('phone_number', 'Phone number:') }}
					{!! Form::text('phone_number', null, ['class' => 'form-control']) !!}
				</div>
				<div class="col-md-12">
					{{ Form::label('message', 'Message:') }}
					{!! Form::textarea('message', null, ['class'=>'form-control']) !!}
				</div>
				<div class="col-md-5 btn-contact-us">
					{{ Form::submit('GỬI LỜI NHẮN', ['class'=>'button-default form-control']) }}
				</div>
			{!! Form::close() !!}
	    </div>
	  </div>
</div>
</div>
@endsection
