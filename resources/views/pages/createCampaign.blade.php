@extends('layouts.app')

@section('content')
<div class="container">
	<div class=" banner-static-pages">
		<h1 class="title-static-page">Tạo chiến dịch</h1>
		<p class="desc-static-page">ViUocMo là mô hình kết nối cộng đồng của người Việt Nam.</p>
	</div>

	<div class="row">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
	</div>
	<div class="row create-campain">
		{{ Form::open(['url' => 'submit-create-campaign', 'files' => true]) }}
		{!! Form::token() !!}
			<div class="col-md-12 part_type_upload">
				{{ Form::label('type_upload', 'Hình thức đăng tải:') }}
				{!! Form::select('type_upload', ['url_youtube' => 'URL Youtube', 'photo' => 'Photo'], null, ['class' => 'form-control select_type_upload', 'placeholder' => 'Slect type upload']) !!}
			</div>
			<div class="col-md-12 part_upload_photo">
				{{ Form::label('upload_photo', 'Hình:') }}
				{!! Form::file('upload_photo', ['class' => 'form-control']) !!}
			</div>
			<div class="col-md-12 part_upload_youtube">
				{{ Form::label('upload_url', 'URL Youtube:') }}
				{!! Form::text('upload_url', null, ['class' => 'form-control']) !!}
			</div>
			<div class="col-md-12">
				{{ Form::label('money_target', 'Số tiền mong muốn:') }}
   				 {!! Form::text('money_target_backup', null, ['class' => 'form-control currency-control-bc']) !!}
    			<input type="hidden" name="money_target" class="currency-control">
			</div>
			<div class="col-md-12">
				{{ Form::label('title', 'Tiêu đề:') }}
				{!! Form::text('title', null, ['class' => 'form-control']) !!}
			</div>
			<div class="col-md-12">
				<?php
					foreach($categories as $cat){
					    $cates[$cat->id] = $cat->title;
					}
				?>
				{{ Form::label('category', 'Thể loại:') }}
				{!! Form::select('category', $cates, null, ['placeholder' => 'Pick a category', 'class' => 'form-control']) !!}
			</div>
			<div class="col-md-12">
				{{ Form::label('content', 'Nội dung:') }}
				{!! Form::textarea('content', null, ['id'=>'tinyMce', 'class'=>'form-control']) !!}
			</div>
			<div class="col-md-5 campain-submit-ctrl">
				{{ Form::submit('Khởi Tạo', ['class'=>'button-default form-control']) }}
			</div>
		{!! Form::close() !!}
	</div>
</div>
@endsection
