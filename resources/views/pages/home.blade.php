@extends('layouts.app')

@section('content')
<div class="home-page-layout">
	<!-- <div class="container-fluid carousel-holder no-padding">
		<div class="col-lg-12 no-padding">
			
		</div>
	</div> -->
	<div id="myCarousel" class="carousel slide home-slider" data-ride="carousel" data-interval="2000" data-wrap="true">
		<!-- Indicators -->
		<!-- Wrapper for slides -->
		<div class="carousel-inner wrapper-img">
			<div class="item active">
				<a href="{{ url('categories/1') }}"> <img src="public/uploads/Slider/1.jpg" alt="Los Angeles" style="width:100%"></a>
			</div>

			<div class="item">
			<a href="{{ url('categories/2') }}"> <img src="public/uploads/Slider/2.jpg" alt="Los Angeles" style="width:100%"></a>
			</div>
		
			<div class="item">
			<a href="{{ url('categories/3') }}"> <img src="public/uploads/Slider/3.jpg" alt="Los Angeles" style="width:100%"></a>
			</div>
			<div class="item">
			<a href="{{ url('categories/4') }}"> <img src="public/uploads/Slider/4.jpg" alt="Los Angeles" style="width:100%"></a>
			</div>
			<div class="item">
			<a href="{{ url('categories/5') }}"> <img src="public/uploads/Slider/5.jpg" alt="Los Angeles" style="width:100%"></a>
			</div>
			<div class="item">
			<a href="{{ url('categories/6') }}"> <img src="public/uploads/Slider/6.jpg" alt="Los Angeles" style="width:100%"></a>
			</div>
			<div class="item">
			<a href="{{ url('categories/7') }}"> <img src="public/uploads/Slider/7.jpg" alt="Los Angeles" style="width:100%"></a>
			</div>
			<div class="item">
			<a href="{{ url('categories/8') }}"> <img src="public/uploads/Slider/8.jpg" alt="Los Angeles" style="width:100%"></a>
			</div>
			<div class="item">
			<a href="{{ url('categories/9') }}"> <img src="public/uploads/Slider/9.jpg" alt="Los Angeles" style="width:100%"></a>
			</div>
		</div>

		<!-- Left and right controls -->
		<a class="left carousel-control" href="#myCarousel" data-slide="prev">
			
			<i class="fa fa-chevron-left" aria-hidden="true"></i>
		</a>
		<a class="right carousel-control" href="#myCarousel" data-slide="next">
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
		<div class="row home-slider-discribe">
			
			<div class=" title-home-wrapper">
				<p class="title-home"> Quyên góp cho bản thân,<br>các tổ chức từ thiện<br>và mục đích khác </p>
				<a class="button-m button-default btn-custome-startfun " href="{{ url('create-campaign') }}">TẠO CHIẾN DỊCH</a>
				<a class="button-m button-default btn-custome-howitwork" href="{{ url('how-it-works') }}">MÔ HÌNH HOẠT ĐỘNG</a>
				
			</div>
			
	</div>


	</div>
	
	<div class="container home-page-content">
		
		
			<div class=" row icon-categroies">
				<div class= "col-md-12 icon-categroies-point">
					<h2> <i class="fa fa-hand-o-right" aria-hidden="true"></i> lớn hay nhỏ, tất cả bắt đầu từ bạn</h2>
				</div>
				<div class= "col-md-12 icon-categroies-details">
					<a class="mini-watch" href="{{ url('categories/1') }}">
					<div class= "icon-cat ">
					<img src="public/uploads/icon-categories/1.png" alt=" ico">					
					</div>
					<h4> Điều Trị Bệnh</h4>
					</a>

					<a class="mini-watch" href="{{ url('categories/2') }}">
					<div class= "icon-cat">
					<img src="public/uploads/icon-categories/2.png" alt=" ico" >					
					</div>
					<h4>Khẩn Cấp</h4></a>

					<a class="mini-watch" href="{{ url('categories/3') }}">
					<div class= "icon-cat">
					<img src="public/uploads/icon-categories/4.png" alt=" ico" >					
					</div><h4>Giáo Dục</h4></a>

					<a class="mini-watch" href="{{ url('categories/4') }}">
					<div class= "icon-cat">
					<img src="public/uploads/icon-categories/3.png" alt=" ico" >					
					</div><h4>Động Vật</h4></a>					

					<a class="mini-watch" href="{{ url('categories/5') }}">
					<div class= "icon-cat">
					<img src="public/uploads/icon-categories/5.png" alt=" ico">					
					</div><h4>Thể Thao</h4></a>

					<a class="mini-watch" href="{{ url('categories/6') }}">
					<div class= "icon-cat">
					<img src="public/uploads/icon-categories/6.png" alt=" ico" >					
					</div><h4>Ước Mơ</h4></a>

					<a class="mini-watch" href="{{ url('categories/7') }}">
					<div class= "icon-cat">
					<img src="public/uploads/icon-categories/9.png" alt=" ico">					
					</div><h4>Từ Thiện</h4></a>

					<a class="mini-watch" href="{{ url('categories/8') }}">
					<div class= "icon-cat">
					<img src="public/uploads/icon-categories/7.png" alt=" ico">					
					</div><h4>Tín Ngưỡng</h4></a>					

					<a class="mini-watch" href="{{ url('categories/9') }}">
					<div class= "icon-cat">
					<img src="public/uploads/icon-categories/8.png" alt=" ico" >					
					</div><h4>Cộng Đồng</h4></a>
				
				</div>
	</div>

	<div class="row trust-my-company">
			<h1 class="title-trust">NỀN TẢNG GÂY QUỸ ĐƯỢC TIN CẬY NHẤT</h1>
			<p class="litle-trust-child">ViUocMo là mô hình kết nối cộng đồng của người Việt Nam.</p>
			<div class="photo-trust"></div>
			<div class="desc-trust">
				<div class="col-md-4 col-xs-4">
					<p class="desc-trust-title">Miễn phí và dễ dàng hoạch định</p>
					<p class="desc-trust-desc">Bắt đầu chiến dịch ngay. Không yêu cầu số tiền tối thiểu hay tối đa. Không có hạn chót.</p>
					<a class="expand" href="{{ url('how-it-works') }}">Cách Hoạt Động</a>
				</div>
				<div class="col-md-4 col-xs-4">
					<p class="desc-trust-title">Luôn được chuyên gia tư vấn</p>
					<p class="desc-trust-desc"> Đội ngũ tư vấn viên và các chuyên gia trong lĩnh vực sẽ phản hồi cho bạn nhanh nhất.</p>
					<a class="expand" href="{{ url('contact-us') }}">Liên Hệ Ngay</a>
				</div>
				<div class="col-md-4 col-xs-4">
					<p class="desc-trust-title">Đảm bảo sự an toàn và riêng tư</p>
					<p class="desc-trust-desc">ViUocMo bảo vệ quyền lợi, hoạt động quyên góp của bạn và những người liên quan.</p>
					<a class="expand" href="#">Vì Ước Mơ</a>
				</div>
			</div>
		</div>

	</div>
	
</div>
@endsection
