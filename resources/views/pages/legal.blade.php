@extends('layouts.app')

@section('content')
<div class="container legal-content-overview">
        <div class=" banner-static-pages">
            <h1 class="title-static-page">Legal</h1>
            <p class="desc-static-page"ViUocMo là mô hình kết nối cộng đồng thế giới.</p>
        </div>

	<div class="button-m button-custome button-static-pages btn-term">Legal Contact Directory</div>

    <div class="legal-content">
        <p>We take your legal questions and concerns very seriously at GoFundMe. For a directory of who to contact for your specific inquiry, please review the sections below.</p>
    </div>

    <div class="button-m button-custome button-static-pages btn-term">Site Issues</div>

    <div class="legal-content">
        <p>If you have a problem concerning an account, how GoFundMe works or any other general issue, please email support@<span style="display:none;">null</span>gofundme.com, or <a href="/support">Contact Customer Support</a>.</p>
        <p>If you are looking to get a campaign removed for fraudulent or untrustworthy behavior, report the campaign <a href="/contact?t=donation_page_report">here</a>.</p>
    </div>

    <div class="button-m button-custome button-static-pages btn-term">Legal Issues</div>

    <div class="legal-content">
        <p>If you are law enforcement and have a court-ordered subpoena for GoFundMe for review, you must submit it via: <a href="https://www.gofundme.com/form/subpoena" target="_blank">https://www.gofundme.com/form/subpoena</a></p>
        <p>All civil subpoenas must be properly localized and served on GoFundMe’s registered agent.</p>
        <p>GoFundMe does not accept service of civil subpoenas via e-mail and will not respond to civil subpoenas delivered by email.  Nor will GoFundMe consider subpoenas that are not properly served</p>
        <p>If you have other law-related questions, please send an email to <a href="mailto:Legal@GoFundMe.com">legal@gofundme.com</a>.</p>
    </div>
</div>
@endsection
