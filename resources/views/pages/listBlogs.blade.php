@extends('layouts.app')

@section('content')
<div class="jumbotron banner-static-pages news-blog">
	<div class="title-desc" >
		<h1 class="title-static-page">list blog</h1>
		<p class="desc-static-page">ViUocMo là mô hình kết nối cộng đồng thế giới.</p>
	</div>
</div>

<div class="container content-news-blog">
	<div class="row title-content">
		<h1>Blogs</h1><hr>
	</div>
	
	<div class="content-news-blog-one">
		@foreach ($blogs as $key => $blogs_items)
			@if ($key %2 == 0)
				<div class="row content-news-blog84">
					<div class="col-md-8 content-news-blog-interface ">
						<img class="img_news" src="{{ url('public/uploads/blogs') }}/{{ $blogs_items->image }}">
					</div>
					<div class="col-md-4 content-news-blog-credit">
						<a href="{{ url('/blogs') }}/{{ $blogs_items->id }}">
							<h4 class="title-news-item">{{ $blogs_items->title }}</h4>
						</a>
						<div class = "whow-up-content">{!! $blogs_items->content !!}</div>
						<div class="Credit-content"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i><span> At ==>> </span> {{ $blogs_items->created_at }}</div>
					</div>
				</div>
			@else
			<div class="row content-news-blog48">
					<div class="col-md-4 content-news-blog-credit">
						<a href="{{ url('/blogs') }}/{{ $blogs_items->id }}">
							<h4 class="title-news-item">{{ $blogs_items->title }}</h4>
						</a>
						<div class = "whow-up-content">{!! $blogs_items->content !!}</div>
						<div class="Credit-content"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i><span> At ==>> </span> {{ $blogs_items->created_at }}</div>
					</div>
					<div class="col-md-8 content-news-blog-interface">
						<img class="img_news" src="{{ url('public/uploads/news') }}/{{ $blogs_items->image }}">
					</div>
				</div>
			@endif
		@endforeach
	</div>
</div>
@endsection
