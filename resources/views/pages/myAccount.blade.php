@extends('layouts.app')

@section('content')
<div class=" container content-page-my-account">
 <div class="  banner-static-pages">
  <h1 class="title-static-page">Thông tin</h1>
  <p class="desc-static-page">ViUocMo là mô hình kết nối cộng đồng của người Việt Nam</p>
 </div>

 <div class="container  profile_user ">
  <div  class="row">
   {{ Form::open(['url' => 'submit-update-profile', 'files' => true]) }}
   {!! Form::token() !!}
    <div class="col-md-7">
     <div class="row">
      {{ Form::label('name', 'Tên :', ['class' => 'col-md-3']) }}
      {!! Form::text('name', $user->name, ['class' => 'col-md-9 formControlProfile']) !!}
     </div>
     <div class="row">
      {{ Form::label('email', 'Email :',['class' => 'col-md-3']) }}
      {!! Form::text('email', $user->email, ['disabled'=>'true', 'class' => 'col-md-9 formControlProfile']) !!}
     </div>
     <div class="row">
      {{ Form::label('sex', 'Giới tính :',['class' => 'col-md-3']) }}
      {!! Form::select('sex', ['male' => 'Male', 'female' => 'Female'], $user->sex, ['class' => 'col-md-9 formControlProfile', 'placeholder' => 'Select your sex']) !!}
     </div>
     <div class="row">
      {{ Form::label('age', 'Tuổi:',['class' => 'col-md-3']) }}
      {!! Form::number('age', $user->age, ['class' => 'col-md-9 formControlProfile']) !!}
     </div>
     <div class="row">
      {{ Form::label('description', 'Mô tả về bản thân:',['class' => 'col-md-3']) }}
      {!! Form::textarea('description', $user->description, ['class'=>'col-md-9 formControlProfile']) !!}
     </div>
     <div class="row">
      {{ Form::submit('GỬI MẪU', ['class' => 'button-m button-default']) }}
     </div>
    </div>
    <div class="col-md-5 profile_user_avatar ">
     
     <div class="avatar_upload_wrapper">
       
      {{ Form::label('avatar', ' ') }}
      {!! Form::file('avatar', ['class' => 'form-control formControlProfile upload-avatar', 'onchange' => 'document.getElementById("avatar_user").src = window.URL.createObjectURL(this.files[0])']) !!}
      <img id="avatar_user" class="picture_effect" src="{{ url('public/uploads/avatars') }}/{{ $user->avatar }}">
      
      <h2 class="name_profile">Thông tin của {{ $user->name }}</h2>
     </div>
     
     <div class="row">
      {{ Form::label('fullname', 'Họ & tên:',['class' => 'col-md-3']) }}
      {!! Form::text('fullname', $user->fullname, ['class' => ' col-md-9 formControlProfile']) !!}

     </div>
     <div class="row">
      {{ Form::label('address', 'Địa chỉ:',['class' => 'col-md-3'] ) }}
      {!! Form::text('address', $user->address, ['class' => ' col-md-9 formControlProfile']) !!}
     </div>
     <div class="row">
      {{ Form::label('mobile', 'Số điện thoại:',['class' => 'col-md-3']) }}
      {!! Form::text('mobile', $user->mobile, ['class' => 'col-md-9 formControlProfile']) !!}
     </div>

    </div>
   {!! Form::close() !!}
  </div>
 </div>
</div>
@endsection