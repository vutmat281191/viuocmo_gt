@extends('layouts.app')

@section('content')
<div class="container control-campain">	
<div class=" banner-static-pages">
	<h1 class="title-static-page">Danh sách chiến dịch của tôi</h1>
	<p class="desc-static-page">ViUocMo là mô hình kết nối cộng đồng thế giới.</p>
</div>

	<div class="row title-campain">
		
	</div>
	<div class="row little-title-campain">
		
	</div>
	<div class="row">
		@foreach ($campaigns as $campaign)
			<div class="campaign-item col-lg-3 col-md-3 col-sm-4 col-xs-6">
				<div class="img-campaign-item">
					<?php
						if ($campaign->type_upload == 'photo') {
							echo '<img class="img_campaign" src="' . url("public/uploads/campaigns/" . $campaign->upload) . '">';
						} else {
							echo '<div class="embed-responsive embed-responsive-16by9 col-xs-12 text-center">' . $campaign->upload . '</div>';
						}
					?>
				</div>
				<h3 class="target-campaign-item">{{ $campaign->money_target }}</h3>
				<div class="campaign-item-bottom">
					<a href="{{ url('/campaign') }}/{{ $campaign->id }}">
						<h4 class="title-campaign-item">{{ $campaign->title }}</h4>
					</a>
					<div class="function-campaign-item">
						<a href="">Edit</a>&nbsp;|&nbsp;
						<a href="javascript:void(0);" onClick="changeStatusCampaign({{ $campaign->id }})">{{ $campaign->status }}</a>
					</div>
				</div>
			</div>
		@endforeach
	</div>
</div>
@endsection
