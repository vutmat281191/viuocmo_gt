@extends('layouts.app')

@section('content')

	@if ($count === 0)
	    <div class="banner-static-pages">
			<h1 class="title-static-page">Tìm kiếm</h1>
			<p class="desc-static-page">Campaign not found.</p>
		</div>
	@else
		<div class="container search-campain-content">
			<div class=" banner-static-pages">
				<h1 class="title-about-us title-static-pages">Tìm Kiếm</h1>
				<p>ViUocMo là mô hình kết nối cộng đồng thế giới.</p>
			</div>
			
			
			<div class="row search-campain-content">
				@foreach ($list as $item)
					<div class="campaign-item col-lg-3 col-md-3 col-sm-4 col-xs-6">
						<div class="img-campaign-item">
							<?php
								if ($item->type_upload == 'photo') {
									echo '<img class="img_campaign" src="' . url("public/uploads/campaigns/" . $item->upload) . '">';
								} else {
									echo '<div class="embed-responsive embed-responsive-16by9 col-xs-12 text-center">' . $item->upload . '</div>';
								}
							?>
						</div>
						<h3 class="target-campaign-item">{{ $item->money_target }} (VNĐ)</h3>
						<div class="campaign-item-bottom">
							<a href="{{ url('/campaign') }}/{{ $item->id }}">
								<h4 class="title-campaign-item">{{ $item->title }}</h4>
							</a>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	@endif
@endsection
