@extends('layouts.app')

@section('content')
<div class="container">
		<div class=" banner-static-pages">
			<h1 class="title-static-page">Cách hoạt động</h1>
			<p class="desc-static-page">ViUocMo là mô hình kết nối cộng đồng của người Việt Nam.</p>
		</div>

		<div class= "row content-howtowork">
			
		
			<div class="myvideo">
				<div class="video-content">
					 <i class="fa fa-hand-o-right" aria-hidden="true"></i><span> Xem video</span>
					<div class="col-md-12 text-center">
					<iframe class ="video-howitwork" src="https://www.youtube.com/embed/tgbNymZ7vqY"></iframe>
					</div>
				</div>
			</div>

			<div class="btn-howitwork">
					<button class="button-m button-default btn-start-campain" href="{{ url('create-campaign') }}"> <span> TẠO CHIẾN DỊCH</span></a> 
			</div>
			<div class="how-it-work-content">
				
		
				<div class=" row step1">

					<div class="col-md-4">
						<div class="button-static-pages"><i class="fa fa-hand-o-right" aria-hidden="true"></i> <h1>Bước 1</h1></div>
					</div>
					<div class="col-md-8"> 	
						<h2 class="tour-step-title">Hãy tạo chiến dịch quyên góp</h2>
						<p class="tour-step-content">There’s no easier way to share your story and attract support.</p>			
					</div>
				</div>

				<div class=" row step2">

					<div class="col-md-4">
						<div class="button-static-pages"><i class="fa fa-hand-o-right" aria-hidden="true"></i> <h1>Bước 2</h1></div>
					</div>
					<div class="col-md-8"> 	
						<h2 class="tour-step-title">Chia sẻ cho bạn bè và người thân được biết</h2>
						<p class="tour-step-content">Our built-in connections to Facebook, Twitter & Email make sharing a breeze.</p>			
					</div>
				</div>

				<div class="row step3">

					<div class="col-md-4">
						<div class="button-static-pages"><i class="fa fa-hand-o-right" aria-hidden="true"></i> <h1>Bước 3</h1></div>
					</div>
					<div class="col-md-8"> 	
						<h2 class="tour-step-title">Thanh toán dễ dàng</h2>
						<p class="tour-step-content">Receive your money by requesting a check or bank transfer.</p>			
					</div>
				</div>

				<div class="row step4">

					<div class="col-md-4">
						<div class="button-static-pages"><i class="fa fa-hand-o-right" aria-hidden="true"></i> <h1>Bước 4</h1></div>
					</div>
					<div class="col-md-8"> 	
						<h2 class="tour-step-title">Tận hưởng thành quả sau khi khởi tạo</h2>
						<p class="tour-step-content">Make changes, post updates and send thank-you notes from your dashboard.</p>			
					</div>
				</div>
			</div>

		</div>
</div>
@endsection
