<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Auth::routes();

// use App\Categories;
// Route::get('test', function() {
// 	$cat = Categories::all();
// 	echo '<pre>';
// 	var_dump($cat);
// 	echo '</pre>';
// });


//google
Route::get('/auth/google', 'Auth\AuthController@redirectToGoogle');
Route::get('/auth/google/callback', 'Auth\AuthController@handleGoogleCallback');


//page
Route::get('/home', 'PagesController@home');
Route::get('/', 'PagesController@home');
Route::get('/how-it-works', 'PagesController@tour');
Route::get('/we-are-hiring', 'PagesController@hiring');
Route::get('/about-us', 'PagesController@aboutUs');
Route::get('/contact-us', 'PagesController@contactUs');
Route::get('/terms', 'PagesController@terms');
Route::get('/privacy', 'PagesController@privacy');
Route::get('/legal', 'PagesController@legal');
Route::get('/fee', 'PagesController@fee');
// Route::get('/news', 'PagesController@news');
// Route::get('/blogs', 'PagesController@blogs');
Route::get('/create-campaign', 'PagesController@pageCreateCampaign');
Route::get('/categories/{id}', 'PagesController@categories');
Route::get('/campaign/{id}', 'PagesController@campaign');
Route::get('/my-account', 'PagesController@myAccount');
Route::get('/my-campaigns', 'PagesController@myCampaigns');
Route::get('/list-news', 'PagesController@listNews');
Route::get('/list-blogs', 'PagesController@listBlogs');
Route::get('/news/{id}', 'PagesController@news');
Route::get('/blogs/{id}', 'PagesController@blogs');
Route::get('/change-password', 'Auth\UpdatePasswordController@index')->name('password.form');


//submit
Route::post('/submit-create-campaign', 'CampaignController@submitCreateCampaign');
Route::post('/submit-update-profile', 'UsersController@updateProfile');
Route::post('/change-password', 'Auth\UpdatePasswordController@update')->name('password.update');
Route::post('/submit-search-campaign', 'CampaignController@searchCampaign');
Route::post('/submit-create-contact-us', 'UsersController@createContactUs');


//ajax
Route::post('/change-status-campaign', 'CampaignController@changeStatusCampaign');


////////admin
//category
Route::get('/admin/category/create', 'Admin\ManageCategoriesController@createCategory');
Route::post('/submit-create-category-admin', 'Admin\ManageCategoriesController@submitCreateCategory');
Route::get('/admin/category/edit/{id}', 'Admin\ManageCategoriesController@editCategory');
Route::post('/submit-edit-category', 'Admin\ManageCategoriesController@submitEditCategory');
Route::get('/admin/category/delete/{id}', 'Admin\ManageCategoriesController@deleteCategory');

//campaign
Route::get('/admin/campaign/create', 'Admin\ManageCampaignsController@createCampaign');
Route::post('/submit-create-campaign-admin', 'Admin\ManageCampaignsController@submitCreateCampaign');
Route::get('/admin/campaign/edit/{id}', 'Admin\ManageCampaignsController@editCampaign');
Route::post('/submit-edit-campaign', 'Admin\ManageCampaignsController@submitEditCampaign');
Route::get('/admin/campaign/delete/{id}', 'Admin\ManageCampaignsController@deleteCampaign');

//news
Route::get('/admin/news/create', 'Admin\ManageNewsController@createNews');
Route::post('/submit-create-news-admin', 'Admin\ManageNewsController@submitCreateNews');
Route::get('/admin/news/edit/{id}', 'Admin\ManageNewsController@editNews');
Route::post('/submit-edit-news', 'Admin\ManageNewsController@submitEditNews');
Route::get('/admin/news/delete/{id}', 'Admin\ManageNewsController@deleteNews');

//blogs
Route::get('/admin/blogs/create', 'Admin\ManageBlogsController@createBlogs');
Route::post('/submit-create-blogs-admin', 'Admin\ManageBlogsController@submitCreateBlogs');
Route::get('/admin/blogs/edit/{id}', 'Admin\ManageBlogsController@editBlogs');
Route::post('/submit-edit-blogs', 'Admin\ManageBlogsController@submitEditBlogs');
Route::get('/admin/blogs/delete/{id}', 'Admin\ManageBlogsController@deleteBlogs');

//contact us
Route::get('/admin/message/delete/{id}', 'Admin\ManageContactUsController@deleteMessage');